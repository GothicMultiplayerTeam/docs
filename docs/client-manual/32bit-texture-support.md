---
title: '32 Bit texture support'
---
!!! info "Available since version: 0.3.0"

**Gothic 2 Online** improves texture quality by adding proper BGRA8888 format readout and DirectX 32 Bit surface support in ZenGin[^1].
This essentially allows server creators to include far superior quality textures into their servers.

## Compilation

ZenGin doesn’t natively support the compilation of such textures. This can only be achieved using external programs. The simplest method to create a BGRA8888 texture is by utilizing [zTEXiPy](https://gitlab.com/Shoun2137/ztexipy).

1. Open your texture in zTEXiPy[^2]:
![zTEXiPy's splash dialog](../assets/32bit-texture-support/STEP_1.WEBP)
2. Choose `Save TEX as...`
3. In this window:
![zTEXiPy's saving dialog](../assets/32bit-texture-support/STEP_3.WEBP)
    - Uncheck `Generate Mipmaps` if your texture is meant to be used as UI.
    - `Colorspace` must be set as `BGRA8888 (zEnum:3)`
4. Press `Save` and check your texture in-game:
![Ingame Example](../assets/32bit-texture-support/EXAMPLE_INGAME.WEBP)

## But why?

This type of texture is compatible with any asset, whether it’s armour or a sword. However, the primary application for such high-quality textures is in user interface elements, which make extensive use of gradients and the alpha channel:

=== "BGRA8888 vs DXT3"

    ![Comparison between BGRA8888 vs DXT3 (Alpha)](../assets/32bit-texture-support/EXAMPLE_1.WEBP)

    This example shows comparison between raw BGRA8888 colorspace and DXT3 (BC2)[^3] compression.
    DXT3 shows noticible banding throughout entire texture wherever there's alpha channel, while BGRA8888 has smooth gradient transition.

=== "BGRA8888 vs DXT1"

    ![Comparison between BGRA8888 vs DXT1](../assets/32bit-texture-support/EXAMPLE_2.WEBP)

    This example shows comparison between raw BGRA8888 colorspace and DXT1 (BC1)[^3] compression.
    DXT1 shows lossy quantization artifacts, while BGRA8888 has none.

[Source comparison files are available here.](../assets/32bit-texture-support/EXAMPLE_SOURCE.7z)

The drawback of using these textures is their large size, as they contain uncompressed color data. Nonetheless, the G2O downloader compresses the data, so it shouldn't matter in the long run as long as you're using your own VDFS addons.

[^1]:
    To learn more about [ZenGin Textures reference click here](https://zk.gothickit.dev/engine/formats/texture/).

[^2]:
    zTEXiPy supports TGA, DDS, PNG and WEBP by default, so make sure it's one of those formats.

[^3]:
    To learn more about [DXTn Algorithm click here](https://www.buckarooshangar.com/flightgear/tut_dds.html).
