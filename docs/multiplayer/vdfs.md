## Gothic VDFS
Gothic uses his own virtual file system also known as VDFS.
The VDFS loads addons with extensions like `.vdf` and `.mod` on game startup, mainly from `Data/` directory.

## Multiplayer VFS
Gothic 2 Online provides extra layer of virtual file system, on top of existing Gothic VDFS.
The additional layer separates addons between servers, thanks to it addons will not override each other.
Every addon can be automatically downloaded and loaded, thanks to built [downloader](../../server-manual/configuration/#downloader).
Downloaded addons are placed into `Multiplayer/Store/IP_Port` or `Multiplayer/Store/Group` directory, in case if server owner wants to share files between servers.

**Important note!** Addons loaded by multiplayer virtual file system has priority over standard ones.
This means that files from standard addons are overwritten by files from addons loaded by multiplayer.

Currently supported extensions: `.vdf`, `.mod`.
