List of modification limitations:

| Name         | Description                          |
| :---------- | :-----------------------------------: |
| Max. players | `512` |
| Max. items on ground | `65565` |
| Max. virtual worlds | `65535` |
| Max. health | `1048576` |
| Max. mana | `1048576` |
| Max. animations | `4094` |
| Player scale | `0.0` - `63.0` |
| Player fatness | `-20.0` - `20.0` |
| Map size | `-640k` - `640k` |
| Description | `400` characters |
| Hostname | `32` characters |
| Nickname | `32` characters |
