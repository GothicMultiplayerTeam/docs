**Gothic 2 Online** stores extra information about player know as *scripting context*.
Scripting context is used to extend data of some multiplayer packets.
This feature helps server owners to reduce network overhead, and most importantly gives possibility to
extend default behaviour of some multiplayer actions.

## Context types
There are several available context types.

### Damage
This context extends packets with damage inflicted information to other players, which are sent from client to server.

### Equipment
This context type extends packets related to player inventory. Currently, context can be attached to equipped or used items.

## The problem
Imagine your goal is to create an MMORPG server. What is such a server without a variety of items, with random effects?
Unfortunately, every item in the game must have a *daedalus script instance*, which requires you to create the same item X times with different effects.
This is not the way.

The second option is to create your own inventory, where the *daedalus script instance* will be mainly the visual aspect, and the rest of the statistics and effects will be added by us in the script.

It is a very flexible solution thanks to which we have full control over our items.
The problem appears only with our custom items synchronization.

Suppose we have twenty dragon blades in our inventory, each has different effects.
By equipping one of them and damaging someone, how is the scripter supposed to know which item was the damage source?
Multiplayer has no idea about our own inventory system, so it sends information only about the item *daedalus script instance*,
which does not contain information about effects.

To solve this problem, we need to set the damage context to our own item ID, which the server can clearly tell which sword was used to deal damage.
