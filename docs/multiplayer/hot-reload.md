Gothic 2 Online supports dynamic reloading of scripts when scripters introduce new changes to specific **.nut** file and save it when server is running. Every script type supports this feature.

#### Configuration

Script files doesn't have hot reloading feature enabled by default.  
To enable it, you need to set `reload` attribute to `true`:
```xml
<script src="example.nut" type="client" reload="true" />
```

#### Reloading

In order to reload a specific script, you just need to save your changes in your text editor.  
There are some things that you need to be aware of when reloading a script:  

- By default reloading a script will only remove every **local** data, like `variables`, `functions`, `classes`, `objects`, etc.
- You as a scripter are responsible for cleaning up the things that aren't defined as **local**, a good example might be [killing a timer](../../script-reference/shared-functions/timer/killTimer), or [unbinding the event handler](../../script-reference/shared-functions/event/removeEventHandler)
- Call [setUnloadCallback](../../script-reference/shared-functions/reload/setUnloadCallback) in your scripts globally to bind a function that will be called before specific script gets unloaded (put your cleanup code there).
- Call [setReloadCallback](../../script-reference/shared-functions/reload/setReloadCallback) in your scripts globally when you need to initialize something when your script gets loaded (put your init code there, e.g: that gets also executed in **onInit** event).
- Every player that is connected to the server while client script gets reloaded will receive the updated version of the code.
- New players that will join a server will also download the updated client scripts containing modified code.


#### Code example

The following example demonstates how you can properly use hot reload callbacks to implement cleanup/init of your script.

```js
local timer_id = -1
local function timer_func()
{
	print("working... " + getTickCount())
}

// this function will be called when script gets initialized for the first time
// or when its reloaded
local function init()
{
	timer_id = setTimer(timer_func, 1000, 0)
}

// this function will be called before script gets unloaded
local function cleanup()
{
	killTimer(timer_id)
}

setReloadCallback(init)
setUnloadCallback(cleanup)
```