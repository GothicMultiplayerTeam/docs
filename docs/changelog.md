## 0.3.1.0

**[FIXED]** Client-side function `isPlayerStreamed` not working correctly for server-side npcs.  

**[UPDATED]** Increased server-side npc health, mana, dexterity, strength statistics limits to `32` bits (previous limit was set to `24` bits)  
**[UPDATED]** Increased server-side npcs max limit to `65535`.  

**[ADDED]** Client-side function `getHostedNpcs`.  
**[ADDED]** Client-side function `getStreamedPlayers`.  
**[ADDED]** Client-side event `onNpcChangeHost`.  