---
title: 'NpcAction'
---
# `class` NpcAction <font size="4">(server-side)</font>
!!! info "Available since version: 0.3.0"

This class represents script NPC action.


## Properties
No properties.

----

## Methods
### onFinished

This method is triggered when NPC action is finished.
Used to execute logic after finishing action.

```cpp
void onFinished(int npc_id, bool result)
```

**Parameters:**

* `int` **npc_id**: the remote npc identifier.
* `bool` **result**: the action result.
  

----
### onSerialize

This method is triggered when server sends NPC action to streamed players.
Used to serialize action data into packet.

```cpp
void onSerialize(instance packet)
```

**Parameters:**

* `instance` **packet**: the packet for writing action data.
  

----
