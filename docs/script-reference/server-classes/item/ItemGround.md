---
title: 'ItemGround'
---
# `static class` ItemGround <font size="4">(server-side)</font>
!!! info "Available since version: 0.1.0"

This class represents item on the ground.


## Properties
### `int` id <font size="2">(read-only)</font>

Represents the unique id of the item ground.

----
### `string` instance <font size="2">(read-only)</font>

Represents the item instance of the item ground.

----
### `int` amount <font size="2">(read-only)</font>

Represents the item amount of item ground.

----
### `string` world <font size="2">(read-only)</font>

Represents the item ground world (.ZEN file path).

----
### `int` virtualWorld 
!!! info "Available since version: 0.1.2"

Represents the virtual world of item ground.

----

## Methods
### getPosition

This method will get the item ground position on the world.

```cpp
{x, y, z} getPosition()
```

  
**Returns `{x, y, z}`:**

the item ground position on the world.

----
### getRotation
!!! info "Available since version: 0.3.0"

This method will get the item ground rotation on the world.

```cpp
{x, y, z} getRotation()
```

  
**Returns `{x, y, z}`:**

the item ground rotation on the world.

----
