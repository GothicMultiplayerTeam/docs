---
title: 'Sky'
---
# `static class` Sky <font size="4">(server-side)</font>
!!! info "Available since version: 0.2.1"

This class represents in game Sky.


## Properties
### `int` weather 

Represents the sky weather. For more information see [Weather constants](../../../shared-constants/weather).

----
### `bool` raining 

Represents the raining/snowing state.

----
### `bool` renderLightning 

Represents the lightning feature during raining state.  
Lightning will only be rendered during raining and when weatherWeight is larger than 0.5

----
### `float` windScale 

Represents the sky wind scale used during raining/snowing.

----
### `bool` dontRain 

Represents the sky dontRain feature.  
When it's enabled, the rain/snow won't fall.

----

## Methods
### setRainStartTime

This method will set the sky weather time when it starts raining/snowing.

```cpp
void setRainStartTime(int hour, int min)
```

**Parameters:**

* `int` **hour**: the sky weather raining start hour.
* `int` **min**: the sky weather raining start min.
  

----
### getRainStartTime

This method will get the sky weather time when it starts raining/snowing.

```cpp
{hour, min} getRainStartTime()
```

  
**Returns `{hour, min}`:**

the sky weather raining start time.

----
### setRainStopTime

This method will set the sky weather time when it stops raining/snowing.

```cpp
void setRainStopTime(int hour, int min)
```

**Parameters:**

* `int` **hour**: the sky weather raining stop hour.
* `int` **min**: the sky weather raining stop min.
  

----
### getRainStopTime

This method will get the sky weather time when it stops raining/snowing.

```cpp
{hour, min} getRainStopTime()
```

  
**Returns `{hour, min}`:**

the sky weather raining stop time.

----
