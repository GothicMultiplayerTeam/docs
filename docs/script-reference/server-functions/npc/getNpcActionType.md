---
title: 'getNpcActionType'
---
# `function` getNpcActionType <font size="4">(server-side)</font>
!!! info "Available since version: 0.3.0"

This function gets numerical type of given script NPC action class.
Every script class derived from `NpcAction` has assigned unique type, which can be retrieved using this function.

## Declaration
```cpp
int getNpcActionType(class action)
```

## Parameters
* `class` **action**: the script NPC action class.
  
## Returns `int`
The registered type of script NPC action. If type is `-1`, then invalid class was provided.

