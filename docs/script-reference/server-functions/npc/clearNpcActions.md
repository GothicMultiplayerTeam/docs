---
title: 'clearNpcActions'
---
# `function` clearNpcActions <font size="4">(server-side)</font>
!!! info "Available since version: 0.3.0"

This function clears remote NPC actions queue.
Remote NPCs uses actions queue to execute thier tasks.

## Declaration
```cpp
void clearNpcActions(int npc_id)
```

## Parameters
* `int` **npc_id**: the npc identifier.
  

