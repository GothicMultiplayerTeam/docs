---
title: 'npcAttackRanged'
---
# `function` npcAttackRanged <font size="4">(server-side)</font>
!!! info "Available since version: 0.3.0"
!!! note
    Required equiped and drawn bow/crossbow.

This function enqueues attack ranged action to the remote NPC action queue.

## Declaration
```cpp
void npcAttackRanged(int attacker_id, int enemy_id, bool turn = false)
```

## Parameters
* `int` **attacker_id**: the remote npc id.
* `int` **enemy_id**: the remote npc or player id.
* `bool` **turn**: pass `true` to turn the attacker toward the enemy, otherwise `false`.
  

