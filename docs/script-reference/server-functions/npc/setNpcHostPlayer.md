---
title: 'setNpcHostPlayer'
---
# `function` setNpcHostPlayer <font size="4">(server-side)</font>
!!! info "Available since version: 0.3.0"

This function sets new NPC host player.

## Declaration
```cpp
bool setNpcHostPlayer(int npc_id, int host_id)
```

## Parameters
* `int` **npc_id**: the npc identifier.
* `int` **host_id**: the player host identifier.
  
## Returns `bool`
`true` if host was successfully changed, otherwise `false`.

