---
title: 'getNpcHostPlayer'
---
# `function` getNpcHostPlayer <font size="4">(server-side)</font>
!!! info "Available since version: 0.3.0"

This function gets NPC host player id.

## Declaration
```cpp
int getNpcHostPlayer(int npc_id)
```

## Parameters
* `int` **npc_id**: the npc identifier.
  
## Returns `int`
the host player identifier. If there is no host player -1 is returned instead.

