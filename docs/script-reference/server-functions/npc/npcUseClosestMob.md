---
title: 'npcUseClosestMob'
---
# `function` npcUseClosestMob <font size="4">(server-side)</font>
!!! info "Available since version: 0.3.0"

This function enqueues use closest mob action to the remote NPC action queue.

## Declaration
```cpp
void npcUseClosestMob(int npc_id, string sceme, int target_state)
```

## Parameters
* `int` **npc_id**: the npc identifier.
* `string` **sceme**: the animation sceme name, e.g: `"BENCH"` when you want to interact with bench.
* `int` **target_state**: the target state, use `1` if you want to start interaction and `-1` to end it.
  

