---
title: 'npcSpellCast'
---
# `function` npcSpellCast <font size="4">(server-side)</font>
!!! info "Available since version: 0.3.0"
!!! note
    Required equiped and setup magic.

This function enqueues spell cast action to the remote NPC action queue.

## Declaration
```cpp
void npcSpellCast(int attacker_id, int enemy_id, bool turn = false)
```

## Parameters
* `int` **attacker_id**: the remote npc id.
* `int` **enemy_id**: the remote npc or player id.
* `bool` **turn**: pass `true` to turn the attacker toward the enemy, otherwise `false`.
  

