---
title: 'npcAttackMelee'
---
# `function` npcAttackMelee <font size="4">(server-side)</font>
!!! info "Available since version: 0.3.0"
!!! note
    Combo is internal Gothic value. Its behaviour can be sometimes undefined. For example -1 value doesn't work for not humanoid NPCs.

This function enqueues attack melee action to the remote NPC action queue.

## Declaration
```cpp
void npcAttackMelee(int attacker_id, int enemy_id, int attack_type, int combo, bool turn = false)
```

## Parameters
* `int` **attacker_id**: the remote npc id.
* `int` **enemy_id**: the remote npc or player id.
* `int` **attack_type**: the type of attack.
* `int` **combo**: the combo sequence. For -1 execute next command immediately.
* `bool` **turn**: pass `true` to turn the attacker toward the enemy, otherwise `false`.
  

