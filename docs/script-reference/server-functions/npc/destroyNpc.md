---
title: 'destroyNpc'
---
# `function` destroyNpc <font size="4">(server-side)</font>
!!! info "Available since version: 0.3.0"

This function destroys remote NPC.

## Declaration
```cpp
bool destroyNpc(int npc_id)
```

## Parameters
* `int` **npc_id**: the identifier of npc.
  
## Returns `bool`
`true` when npc was successfully destroyed, otherwise `false`.

