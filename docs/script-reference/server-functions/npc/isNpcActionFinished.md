---
title: 'isNpcActionFinished'
---
# `function` isNpcActionFinished <font size="4">(server-side)</font>
!!! info "Available since version: 0.3.0"

This function checks whether specified NPC action was finished.

## Declaration
```cpp
bool isNpcActionFinished(int npc_id, int action_id)
```

## Parameters
* `int` **npc_id**: the npc identifier.
* `int` **action_id**: the unique action identifier.
  
## Returns `bool`
`true` if specified action identifier was already finished, otherwise `false`.

