---
title: 'getNpcAction'
---
# `function` getNpcAction <font size="4">(server-side)</font>
!!! info "Available since version: 0.3.0"

This function gets information about element on specified index in NPC action queue.

## Declaration
```cpp
{type, id, status}|null getNpcAction(int npc_id, int index)
```

## Parameters
* `int` **npc_id**: the npc identifier.
* `int` **index**: the index of element in the queue.
  
## Returns `{type, id, status}|null`
The table containing information about selected element, otherwise `null`.

