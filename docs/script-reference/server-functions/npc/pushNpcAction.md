---
title: 'pushNpcAction'
---
# `function` pushNpcAction <font size="4">(server-side)</font>
!!! info "Available since version: 0.3.0"
!!! note
    Only classes derived from `NpcAction` can be pushed!

This function enqueues NPC action to the remote NPC action queue.

## Declaration
```cpp
int pushNpcAction(int npc_id, instance action)
```

## Parameters
* `int` **npc_id**: the npc identifier.
* `instance` **action**: the script NPC action instance.
  
## Returns `int`
The assigned action unique identifier, otherwise `-1`.

