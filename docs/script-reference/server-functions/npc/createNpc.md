---
title: 'createNpc'
---
# `function` createNpc <font size="4">(server-side)</font>
!!! info "Available since version: 0.3.0"
!!! note
    By default npcs won't be added to world. In order to do that, you have to call [spawnPlayer](../../player/spawnPlayer/).
!!! note
    Remote NPC id will always begins from max slots value.

This function creates remote NPC.

## Declaration
```cpp
int createNpc(string name, string instance = "PC_HERO")
```

## Parameters
* `string` **name**: the displayed name of the npc.
* `string` **instance**: the instance name of for the npc.
  
## Returns `int`
The identifier of created npc. If identifier is set to `-1` then creation of npc failed.

