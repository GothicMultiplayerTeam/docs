---
title: 'getNpcActionsCount'
---
# `function` getNpcActionsCount <font size="4">(server-side)</font>
!!! info "Available since version: 0.3.0"

This function gets elements count in NPC action queue.

## Declaration
```cpp
int getNpcActionsCount(int npc_id)
```

## Parameters
* `int` **npc_id**: the npc identifier.
  
## Returns `int`
The count of elements inside queue, otherwise `-1`.

