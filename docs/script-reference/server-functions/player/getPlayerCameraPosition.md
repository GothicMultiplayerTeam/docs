---
title: 'getPlayerCameraPosition'
---
# `function` getPlayerCameraPosition <font size="4">(server-side)</font>
!!! info "Available since version: 0.2.0"

This function will get the player camera position in world.

## Declaration
```cpp
Vec3|null getPlayerCameraPosition(int id)
```

## Parameters
* `int` **id**: the player id.
  
## Returns `Vec3|null`
the vector that represents camera position, or `null` if player isn't created.

