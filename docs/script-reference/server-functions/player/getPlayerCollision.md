---
title: 'getPlayerCollision'
---
# `function` getPlayerCollision <font size="4">(server-side)</font>
!!! info "Available since version: 0.2"

This function will get the player/npc collision.

## Declaration
```cpp
bool|null getPlayerCollision(int id)
```

## Parameters
* `int` **id**: the player id.
  
## Returns `bool|null`
`true` if collision is enabled, otherwise `false` or `null` if player isn't created.

