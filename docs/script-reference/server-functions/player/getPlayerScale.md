---
title: 'getPlayerScale'
---
# `function` getPlayerScale <font size="4">(server-side)</font>
!!! info "Available since version: 0.0.1"

This function will get the player/npc scale.

## Declaration
```cpp
{x, y, z}|null getPlayerScale(int id)
```

## Parameters
* `int` **id**: the player id.
  
## Returns `{x, y, z}|null`
the player scale or `null` if player isn't created.

