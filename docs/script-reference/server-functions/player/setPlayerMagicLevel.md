---
title: 'setPlayerMagicLevel'
---
# `function` setPlayerMagicLevel <font size="4">(server-side)</font>
!!! danger "Deprecated since version: 0.3.0"

This function will set the player/npc magic level for all players.

## Declaration
```cpp
void setPlayerMagicLevel(int id, int magicLevel)
```

## Parameters
* `int` **id**: the player id.
* `int` **magicLevel**: the magic level.
  

