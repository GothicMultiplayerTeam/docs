---
title: 'setPlayerMana'
---
# `function` setPlayerMana <font size="4">(server-side)</font>
!!! info "Available since version: 0.1.3"

This function will set the player/npc mana points for all players.

## Declaration
```cpp
void setPlayerMana(int id, int mana)
```

## Parameters
* `int` **id**: the player id.
* `int` **mana**: the mana points amount.
  

