---
title: 'getPlayerIP'
---
# `function` getPlayerIP <font size="4">(server-side)</font>
!!! info "Available since version: 0.0.1"

This function will get the player ipv4 ip address.

## Declaration
```cpp
string getPlayerIP(int id)
```

## Parameters
* `int` **id**: the player id.
  
## Returns `string`
the player ip address, e.g `"127.0.0.1"` or `null` if player isn't created.

