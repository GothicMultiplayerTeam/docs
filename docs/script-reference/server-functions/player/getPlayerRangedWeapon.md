---
title: 'getPlayerRangedWeapon'
---
# `function` getPlayerRangedWeapon <font size="4">(server-side)</font>
!!! info "Available since version: 0.0.1"

This function will get the equipped player/npc ranged weapon.

## Declaration
```cpp
string|null getPlayerRangedWeapon(int id)
```

## Parameters
* `int` **id**: the player id.
  
## Returns `string|null`
the item instance from Daedalus scripts or `null`.

