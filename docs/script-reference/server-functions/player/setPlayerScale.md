---
title: 'setPlayerScale'
---
# `function` setPlayerScale <font size="4">(server-side)</font>
!!! info "Available since version: 0.0.1"

This function will set the player/npc scale for all players.

## Declaration
```cpp
void setPlayerScale(int id, float x, float y, float z)
```

## Parameters
* `int` **id**: the player id.
* `float` **x**: the scale factor on x axis.
* `float` **y**: the scale factor on y axis.
* `float` **z**: the scale factor on z axis.
  

