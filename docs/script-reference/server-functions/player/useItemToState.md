---
title: 'useItemToState'
---
# `function` useItemToState <font size="4">(server-side)</font>
!!! info "Available since version: 0.2.1"

This function will try to use, interact, open item in specific state by player/npc.

## Declaration
```cpp
void useItemToState(int id, string instance, int state)
```

## Parameters
* `int` **id**: the player id.
* `string` **instance**: the item instance from Daedalus scripts.
* `int` **state**: the state that you'll start from interacting with item.
  

