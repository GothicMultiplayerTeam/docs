---
title: 'removeWeapon'
---
# `function` removeWeapon <font size="4">(server-side)</font>
!!! info "Available since version: 0.3.0"

This function will cause player/npc to hide a weapon.

## Declaration
```cpp
void removeWeapon(int id)
```

## Parameters
* `int` **id**: the player id.
  

