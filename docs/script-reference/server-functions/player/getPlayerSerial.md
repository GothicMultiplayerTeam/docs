---
title: 'getPlayerSerial'
---
# `function` getPlayerSerial <font size="4">(server-side)</font>
!!! info "Available since version: 0.0.1"
!!! note
    The function can return `null` if player isn't connected.
!!! note
    For some players (e.g: that are playing on linux using WINE) this function might return empty string.

This function will get the player serial.  
Serial is used to uniquely idientify each player.

## Declaration
```cpp
string getPlayerSerial(int id)
```

## Parameters
* `int` **id**: the player id.
  
## Returns `string`
the player serial.

