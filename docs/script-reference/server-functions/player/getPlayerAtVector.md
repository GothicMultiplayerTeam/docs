---
title: 'getPlayerAtVector'
---
# `function` getPlayerAtVector <font size="4">(server-side)</font>
!!! info "Available since version: 0.2"

This function will get player/npc at vector.

## Declaration
```cpp
Vec3|null getPlayerAtVector(int id)
```

## Parameters
* `int` **id**: the player id.
  
## Returns `Vec3|null`
the player at vector or `null` if player isn't created.

