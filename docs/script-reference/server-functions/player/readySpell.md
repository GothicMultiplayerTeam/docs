---
title: 'readySpell'
---
# `function` readySpell <font size="4">(server-side)</font>
!!! info "Available since version: 0.3.0"

This function will cause player/npc to ready equipped spell.

## Declaration
```cpp
void readySpell(int id, int slotId, int manaInvested)
```

## Parameters
* `int` **id**: the player id.
* `int` **slotId**: the equipped spell slotId in range <0, 6>.
* `int` **manaInvested**: the spell cast cost in mana points.
  

