---
title: 'setPlayerColor'
---
# `function` setPlayerColor <font size="4">(server-side)</font>
!!! info "Available since version: 0.0.1"

This function will set the player/npc nickname color for all players.

## Declaration
```cpp
void setPlayerColor(int id, int r, int g, int b)
```

## Parameters
* `int` **id**: the player id.
* `int` **r**: the red color component in RGB model.
* `int` **g**: the green color component in RGB model.
* `int` **b**: the blue color component in RGB model.
  

