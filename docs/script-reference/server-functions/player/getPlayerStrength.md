---
title: 'getPlayerStrength'
---
# `function` getPlayerStrength <font size="4">(server-side)</font>
!!! info "Available since version: 0.0.1"

This function will get the player/npc strength points.

## Declaration
```cpp
int|null getPlayerStrength(int id)
```

## Parameters
* `int` **id**: the player id.
  
## Returns `int|null`
the strength points amount or `null` if player isn't created.

