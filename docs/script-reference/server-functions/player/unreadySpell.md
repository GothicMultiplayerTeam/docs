---
title: 'unreadySpell'
---
# `function` unreadySpell <font size="4">(server-side)</font>
!!! info "Available since version: 0.3.0"

This function will cause player/npc to unready active spell.
It works almost the same as [removeWeapon](../removeWeapon), but also stops hero if he's moving before hiding the active spell.

## Declaration
```cpp
void unreadySpell(int id)
```

## Parameters
* `int` **id**: the player id.
  

