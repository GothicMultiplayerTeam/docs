---
title: 'setPlayerRespawnTime'
---
# `function` setPlayerRespawnTime <font size="4">(server-side)</font>
!!! info "Available since version: 0.0.2"
!!! note
    The respawnTime can't be smaller than 1001 miliseconds.

This function will set the player time to respawn after death.
If set to 0, respawn is disabled for selected player.

## Declaration
```cpp
void setPlayerRespawnTime(int id, int respawnTime)
```

## Parameters
* `int` **id**: the player id.
* `int` **respawnTime**: the new respawn time in miliseconds.
  

