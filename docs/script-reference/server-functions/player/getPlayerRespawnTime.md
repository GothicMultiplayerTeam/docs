---
title: 'getPlayerRespawnTime'
---
# `function` getPlayerRespawnTime <font size="4">(server-side)</font>
!!! info "Available since version: 0.0.2"

This function will get the player time to respawn after death.

## Declaration
```cpp
int|null getPlayerRespawnTime(int id)
```

## Parameters
* `int` **id**: the player id.
  
## Returns `int|null`
the player respawn time or `null` if player isn't created.

