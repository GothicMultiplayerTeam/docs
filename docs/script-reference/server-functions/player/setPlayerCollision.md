---
title: 'setPlayerCollision'
---
# `function` setPlayerCollision <font size="4">(server-side)</font>
!!! info "Available since version: 0.2"

This function will set the player/npc collision.

## Declaration
```cpp
void setPlayerCollision(int id, bool collision)
```

## Parameters
* `int` **id**: the player id.
* `bool` **collision**: `true` if want to enable collision, otherwise `false`.
  

