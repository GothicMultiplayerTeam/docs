---
title: 'setPlayerHealth'
---
# `function` setPlayerHealth <font size="4">(server-side)</font>
!!! info "Available since version: 0.0.1"

This function will set the player/npc health points for all players.

## Declaration
```cpp
void setPlayerHealth(int id, int the)
```

## Parameters
* `int` **id**: the player id.
* `int` **the**: health points amount.
  

