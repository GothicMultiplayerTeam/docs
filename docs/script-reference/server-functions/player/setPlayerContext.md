---
title: 'setPlayerContext'
---
# `function` setPlayerContext <font size="4">(server-side)</font>
!!! info "Available since version: 0.2.1"

This function is used to set player/npc script context.
For more information see [this article](../../../../multiplayer/script-context/).

## Declaration
```cpp
void setPlayerContext(int id, int type, int value)
```

## Parameters
* `int` **id**: the player id.
* `int` **type**: the type of modified context.
* `int` **value**: the new value written into context.
  

