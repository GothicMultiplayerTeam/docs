---
title: 'setPlayerFatness'
---
# `function` setPlayerFatness <font size="4">(server-side)</font>
!!! info "Available since version: 0.0.1"

This function will set the player/npc fatness factor for all players.

## Declaration
```cpp
void setPlayerFatness(int id, float fatness)
```

## Parameters
* `int` **id**: the player id.
* `float` **fatness**: ratio of how much you want to make player fatter, `0.0` is default fatness (none).
  

