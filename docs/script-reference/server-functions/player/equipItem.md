---
title: 'equipItem'
---
# `function` equipItem <font size="4">(server-side)</font>
!!! info "Available since version: 0.0.4"
!!! note
    If you want to equip weapon/shield, first make sure that player is in `WEAPONMODE_NONE`.

This function is used to equip item on player/npc for all players.

## Declaration
```cpp
void equipItem(int id, string instance, int slotId = -1)
```

## Parameters
* `int` **id**: the player id.
* `string` **instance**: the item instance from Daedalus scripts.
* `int` **slotId**: the slot id in which you want to equip item on player, e.g scrolls, runes, rings, by default the item will be equipped on the first free slot.
  

