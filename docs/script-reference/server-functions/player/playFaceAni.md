---
title: 'playFaceAni'
---
# `function` playFaceAni <font size="4">(server-side)</font>
!!! info "Available since version: 0.2.1"

This function is used to play face animation on player/npc.

## Declaration
```cpp
void playFaceAni(int id, string aniName)
```

## Parameters
* `int` **id**: the player id.
* `string` **aniName**: the name of the animation, e.g: `"S_FRIENDLY"`.
  

