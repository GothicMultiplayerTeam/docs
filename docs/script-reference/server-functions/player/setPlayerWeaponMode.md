---
title: 'setPlayerWeaponMode'
---
# `function` setPlayerWeaponMode <font size="4">(server-side)</font>
!!! info "Available since version: 0.0.1"

This function will set the player/npc weapon mode for all players.

## Declaration
```cpp
void setPlayerWeaponMode(int id, int weaponMode)
```

## Parameters
* `int` **id**: the player id.
* `int` **weaponMode**: . For more information see [Weapon mode constants](../../../shared-constants/weapon-mode/).
  

