---
title: 'getPlayerMagicLevel'
---
# `function` getPlayerMagicLevel <font size="4">(server-side)</font>
!!! danger "Deprecated since version: 0.3.0"

This function will get the player/npc magic level.

## Declaration
```cpp
int|null getPlayerMagicLevel(int id)
```

## Parameters
* `int` **id**: the player id.
  
## Returns `int|null`
the player magic level or `null` if player isn't created.

