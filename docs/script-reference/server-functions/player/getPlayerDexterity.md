---
title: 'getPlayerDexterity'
---
# `function` getPlayerDexterity <font size="4">(server-side)</font>
!!! info "Available since version: 0.0.1"

This function will get the player/npc dexterity points.

## Declaration
```cpp
int|null getPlayerDexterity(int id)
```

## Parameters
* `int` **id**: the player id.
  
## Returns `int|null`
the dexterity points amount or `null` if player isn't created.

