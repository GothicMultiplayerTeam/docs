---
title: 'getPlayerMacAddr'
---
# `function` getPlayerMacAddr <font size="4">(server-side)</font>
!!! info "Available since version: 0.0.1"
!!! note
    The function can return `null` if player isn't connected.

This function will get the player MAC address.  
MAC is used to uniquely idientify each player,  
however it can be changed/spoofed by more advance users.

## Declaration
```cpp
string getPlayerMacAddr(int id)
```

## Parameters
* `int` **id**: the player id.
  
## Returns `string`
the player mac address, e.g `"00-1b-44-11-3a-b7"`.

