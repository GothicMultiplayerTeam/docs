---
title: 'unequipItem'
---
# `function` unequipItem <font size="4">(server-side)</font>
!!! info "Available since version: 0.0.4"
!!! note
    If you want to unequip weapon/shield, first make sure that player is in `WEAPONMODE_NONE`.

This function is used to unequip item from player/npc for all players.

## Declaration
```cpp
void unequipItem(int id, string instance)
```

## Parameters
* `int` **id**: the player id.
* `string` **instance**: the item instance from Daedalus scripts.
  

