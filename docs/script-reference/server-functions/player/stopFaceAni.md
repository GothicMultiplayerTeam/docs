---
title: 'stopFaceAni'
---
# `function` stopFaceAni <font size="4">(server-side)</font>
!!! info "Available since version: 0.2"

This function is used to stop played face animation on player/npc.

## Declaration
```cpp
void stopFaceAni(int id, string aniName = "" the name of the animation that you want to stop. The default value is `")
```

## Parameters
* `int` **id**: the player id.
* `string` **aniName**: "` which means that the first active ani will be stopped.
  

