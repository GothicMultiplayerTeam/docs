---
title: 'getPlayerVirtualWorld'
---
# `function` getPlayerVirtualWorld <font size="4">(server-side)</font>
!!! info "Available since version: 0.1.2"

This function will get the player/npc virtual world.

## Declaration
```cpp
int|null getPlayerVirtualWorld(int id)
```

## Parameters
* `int` **id**: the player id.
  
## Returns `int|null`
the player virtual world id or `null` if player isn't created.

