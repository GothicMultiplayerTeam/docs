---
title: 'getPlayerName'
---
# `function` getPlayerName <font size="4">(server-side)</font>
!!! info "Available since version: 0.0.1"

This function will get the player/npc nickname.

## Declaration
```cpp
string|null getPlayerName(int id)
```

## Parameters
* `int` **id**: the player id.
  
## Returns `string|null`
the player nickname or `null` if player isn't created.

