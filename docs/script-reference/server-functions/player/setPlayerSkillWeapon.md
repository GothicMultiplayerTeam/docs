---
title: 'setPlayerSkillWeapon'
---
# `function` setPlayerSkillWeapon <font size="4">(server-side)</font>
!!! info "Available since version: 0.0.1"

This function will set the player/npc skill weapon for all players.

## Declaration
```cpp
void setPlayerSkillWeapon(int id, int skillId, int percentage)
```

## Parameters
* `int` **id**: the player id.
* `int` **skillId**: . For more information see [Skill weapon constants](../../../shared-constants/skill-weapon/).
* `int` **percentage**: the percentage in range <0, 100>.
  

