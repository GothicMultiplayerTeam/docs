---
title: 'setPlayerWorld'
---
# `function` setPlayerWorld <font size="4">(server-side)</font>
!!! info "Available since version: 0.0.4"

This function will set the player/npc world for all players.

## Declaration
```cpp
void setPlayerWorld(int id, string world, string startPointName)
```

## Parameters
* `int` **id**: the player id.
* `string` **world**: the path to the target world (.ZEN). World path is relative to directory `_Work/Data/Worlds`.
* `string` **startPointName**: ="" the name of the vob to which the player will be moved. If passed empty string, player will be placed at world start point. If vob with specified name doesn't exists or world doesn't have start point, player will be placed at {0, 150, 0} coordinates.
  

