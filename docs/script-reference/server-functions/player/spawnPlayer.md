---
title: 'spawnPlayer'
---
# `function` spawnPlayer <font size="4">(server-side)</font>
!!! info "Available since version: 0.0.1"
!!! note
    Unspawned players can't see other players, items, etc. and are invisible for others.

This function will spawn the player.  
Players are always in unspawned state after joining to server or after respawning.

## Declaration
```cpp
void spawnPlayer(int id)
```

## Parameters
* `int` **id**: the player id.
  

