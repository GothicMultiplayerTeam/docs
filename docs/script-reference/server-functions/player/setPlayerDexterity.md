---
title: 'setPlayerDexterity'
---
# `function` setPlayerDexterity <font size="4">(server-side)</font>
!!! info "Available since version: 0.0.1"

This function will set the player/npc dexterity points for all players.

## Declaration
```cpp
void setPlayerDexterity(int id, int dexterity)
```

## Parameters
* `int` **id**: the player id.
* `int` **dexterity**: the dexterity points amount.
  

