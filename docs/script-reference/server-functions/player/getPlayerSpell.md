---
title: 'getPlayerSpell'
---
# `function` getPlayerSpell <font size="4">(server-side)</font>
!!! info "Available since version: 0.1.10"

This function will get the equipped player/npc spell.

## Declaration
```cpp
string|null getPlayerSpell(int id, int slotId)
```

## Parameters
* `int` **id**: the player id.
* `int` **slotId**: the equipped slotId in range <0, 6>.
  
## Returns `string|null`
the item instance from Daedalus scripts or `null`.

