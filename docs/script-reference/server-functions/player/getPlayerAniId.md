---
title: 'getPlayerAniId'
---
# `function` getPlayerAniId <font size="4">(server-side)</font>
!!! info "Available since version: 0.0.1"

This function will get the active player/npc animation id.

## Declaration
```cpp
int|null getPlayerAniId(int id)
```

## Parameters
* `int` **id**: the player id.
  
## Returns `int|null`
the ani id or `null` if player isn't created.

