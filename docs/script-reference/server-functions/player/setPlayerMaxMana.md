---
title: 'setPlayerMaxMana'
---
# `function` setPlayerMaxMana <font size="4">(server-side)</font>
!!! info "Available since version: 0.1.3"

This function will set the player/npc max mana points for all players.

## Declaration
```cpp
void setPlayerMaxMana(int id, int maxMana)
```

## Parameters
* `int` **id**: the player id.
* `int` **maxMana**: the maximum mana points amount.
  

