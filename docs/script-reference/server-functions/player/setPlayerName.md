---
title: 'setPlayerName'
---
# `function` setPlayerName <font size="4">(server-side)</font>
!!! info "Available since version: 0.0.1"
!!! note
    The name string can't be longer than 18 characters, and must be unique for each player.

This function will set the player/npc unique nickname for all players.

## Declaration
```cpp
bool setPlayerName(int id, string name)
```

## Parameters
* `int` **id**: the player id.
* `string` **name**: the new unique player name.
  
## Returns `bool`
`true` when unique player name was set, otherwise `false`.

