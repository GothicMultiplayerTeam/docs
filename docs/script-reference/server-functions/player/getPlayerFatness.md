---
title: 'getPlayerFatness'
---
# `function` getPlayerFatness <font size="4">(server-side)</font>
!!! info "Available since version: 0.0.1"

This function will get the player/npc fatness factor.

## Declaration
```cpp
float|null getPlayerFatness(int id)
```

## Parameters
* `int` **id**: the player id.
  
## Returns `float|null`
the fatness ratio or `null` if player isn't created.

