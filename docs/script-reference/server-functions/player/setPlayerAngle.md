---
title: 'setPlayerAngle'
---
# `function` setPlayerAngle <font size="4">(server-side)</font>
!!! info "Available since version: 0.0.1"

This function will set the player/npc facing rotation on y axis for all players.

## Declaration
```cpp
void setPlayerAngle(int id, float angle)
```

## Parameters
* `int` **id**: the player id.
* `float` **angle**: the facing rotation on y axis.
  

