---
title: 'getPlayerMaxHealth'
---
# `function` getPlayerMaxHealth <font size="4">(server-side)</font>
!!! info "Available since version: 0.0.1"

This function will get the player/npc max health points.

## Declaration
```cpp
int|null getPlayerMaxHealth(int id)
```

## Parameters
* `int` **id**: the player id.
  
## Returns `int|null`
the maximum health points amount or `null` if player isn't created.

