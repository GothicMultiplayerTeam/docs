---
title: 'useItem'
---
# `function` useItem <font size="4">(server-side)</font>
!!! info "Available since version: 0.2"

This function will try to use, interact, open item by player/npc.

## Declaration
```cpp
void useItem(int id, string instance)
```

## Parameters
* `int` **id**: the player id.
* `string` **instance**: the item instance from Daedalus scripts.
  

