---
title: 'setPlayerStrength'
---
# `function` setPlayerStrength <font size="4">(server-side)</font>
!!! info "Available since version: 0.0.1"

This function will set the player/npc strength points for all players.

## Declaration
```cpp
void setPlayerStrength(int id, int the)
```

## Parameters
* `int` **id**: the player id.
* `int` **the**: strength points amount.
  

