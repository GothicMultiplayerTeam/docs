---
title: 'getPlayerInvisible'
---
# `function` getPlayerInvisible <font size="4">(server-side)</font>
!!! info "Available since version: 0.1.5"

This function will get the player/npc invisiblity for all players.

## Declaration
```cpp
bool|null getPlayerInvisible(int id)
```

## Parameters
* `int` **id**: the player id.
  
## Returns `bool|null`
`true` when player is invisible for all players, otherwise `false` or `null` if player isn't created.

