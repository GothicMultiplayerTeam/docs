---
title: 'setPlayerVirtualWorld'
---
# `function` setPlayerVirtualWorld <font size="4">(server-side)</font>
!!! info "Available since version: 0.1.2"

This function will set the player/npc virtual world for all players.  
Virtual worlds are separate logical worlds on the same physical world.

## Declaration
```cpp
void setPlayerVirtualWorld(int id, int virtualWorld)
```

## Parameters
* `int` **id**: the player id.
* `int` **virtualWorld**: the virtual world id in range <0, 65535>.
  

