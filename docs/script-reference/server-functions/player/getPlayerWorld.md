---
title: 'getPlayerWorld'
---
# `function` getPlayerWorld <font size="4">(server-side)</font>
!!! info "Available since version: 0.0.4"

This function will get the player/npc world.

## Declaration
```cpp
string|null getPlayerWorld(int id)
```

## Parameters
* `int` **id**: the player id.
  
## Returns `string|null`
the player world or `null` if player isn't created.

