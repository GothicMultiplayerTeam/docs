---
title: 'drawWeapon'
---
# `function` drawWeapon <font size="4">(server-side)</font>
!!! info "Available since version: 0.3.0"

This function will cause player/npc to draw a weapon.
If hero/npc doesn't have equipped weapon assosiated with the preffered weapon mode, then it will try to draw melee weapon, otherwise `WEAPONMODE_FIST` will be used instead.

## Declaration
```cpp
void drawWeapon(int id, int weaponMode)
```

## Parameters
* `int` **id**: the player id.
* `int` **weaponMode**: the preffered weapon mode. For more information see [Weapon mode constants](../../../shared-constants/weapon-mode/).
  

