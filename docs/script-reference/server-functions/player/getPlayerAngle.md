---
title: 'getPlayerAngle'
---
# `function` getPlayerAngle <font size="4">(server-side)</font>
!!! info "Available since version: 0.0.1"

This function will get the player/npc facing rotation on y axis.

## Declaration
```cpp
float|null getPlayerAngle(int id)
```

## Parameters
* `int` **id**: the player id.
  
## Returns `float|null`
the facing rotation on y axis or `null` if player isn't created.

