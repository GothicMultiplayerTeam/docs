---
title: 'setPlayerInstance'
---
# `function` setPlayerInstance <font size="4">(server-side)</font>
!!! info "Available since version: 0.0.1"

This function will set the player/npc instance for all players.
Instance describes the player attributes, like visual, stats, and more..
You can find more information about npc instances in daedalus scripts.

## Declaration
```cpp
void setPlayerInstance(int id, string instance)
```

## Parameters
* `int` **id**: the player id.
* `string` **instance**: the new player instance.
  

