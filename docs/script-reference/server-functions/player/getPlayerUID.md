---
title: 'getPlayerUID'
---
# `function` getPlayerUID <font size="4">(server-side)</font>
!!! info "Available since version: 0.2.1"
!!! note
    The function can return `null` if player isn't connected.

This function will get the player pc unique identifier.

## Declaration
```cpp
string getPlayerUID(int id)
```

## Parameters
* `int` **id**: the player id.
  
## Returns `string`
the player UID.

