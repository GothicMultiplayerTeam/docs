---
title: 'exit'
---
# `function` exit <font size="4">(server-side)</font>
!!! info "Available since version: 0.1.10"

This function will close the server with specified exit code.

## Declaration
```cpp
void exit(int exitCode = 0)
```

## Parameters
* `int` **exitCode**: exit status for g2o server.
  

