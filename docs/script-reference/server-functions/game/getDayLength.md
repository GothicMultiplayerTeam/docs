---
title: 'getDayLength'
---
# `function` getDayLength <font size="4">(server-side)</font>
!!! info "Available since version: 0.1.10"

The function is used to get the day length in miliseconds.

## Declaration
```cpp
float getDayLength()
```

## Parameters
No parameters.
  
## Returns `float`
the current day length in miliseconds.

