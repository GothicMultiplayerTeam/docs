---
title: 'serverLog'
---
# `function` serverLog <font size="4">(server-side)</font>
!!! info "Available since version: 0.3.0"

This function will log the text into server.log file.

## Declaration
```cpp
void serverLog(string text)
```

## Parameters
* `string` **text**: the text message that you want to append to server.log file.
  

