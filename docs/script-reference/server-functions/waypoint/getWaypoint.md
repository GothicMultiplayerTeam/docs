---
title: 'getWaypoint'
---
# `function` getWaypoint <font size="4">(server-side)</font>
!!! info "Available since version: 0.1.4"

This function is used to retrieve the position of specified waypoint.

## Declaration
```cpp
Vec3 getWaypoint(string world, string name)
```

## Parameters
* `string` **world**: the world name in which the waypoint exists.
* `string` **name**: the name of the waypoint.
  
## Returns `Vec3`
The position of waypoint.

