---
title: 'getNearestWaypoint'
---
# `function` getNearestWaypoint <font size="4">(server-side)</font>
!!! info "Available since version: 0.1.4"

This function is used to retrieve the information about nearest waypoint from the specified position.

## Declaration
```cpp
{name, x, y, z} getNearestWaypoint(string world, int x, int y, int z)
```

## Parameters
* `string` **world**: the world name in which the waypoint exists.
* `int` **x**: the position in the world on the x axis.
* `int` **y**: the position in the world on the y axis.
* `int` **z**: the position in the world on the z axis.
  
## Returns `{name, x, y, z}`
Waypoint information.

