---
title: 'Vec4'
---
# `class` Vec4 <font size="4">(shared-side)</font>
!!! info "Available since version: 0.2"

This class represents 4d vector.

### Constructor
```cpp
Vec4()
```

**Parameters:**

No parameters.
### Constructor
```cpp
Vec4(float value)
```

**Parameters:**

* `float` **value**: the initial value for x, y, z, w components.
### Constructor
```cpp
Vec4(float x, float y, float z, float w)
```

**Parameters:**

* `float` **x**: the initial value for x component.
* `float` **y**: the initial value for y component.
* `float` **z**: the initial value for z component.
* `float` **w**: the initial value for w component.

## Properties
### `float` x 

Represents the x component of the vector.

----
### `float` y 

Represents the y component of the vector.

----
### `float` z 

Represents the z component of the vector.

----
### `float` w 

Represents the w component of the vector.

----

## Methods
### len

This method will return the vector length.
The length of the vector is square root of `(x*x + y*y + z*z + w*w)`.

```cpp
float len()
```

  
**Returns `float`:**

the squared length of the vector.

----
### len2

This method will get the squared vector length.
The length of the vector is `(x*x + y*y + z*z + w*w)`.
Calculating the squared length instead of the [len](#len) is much faster.

```cpp
float len2()
```

  
**Returns `float`:**

the length of the vector.

----
### lenApprox

This method will get the approximate vector length.
The length of the vector is approximation square root of `(x*x + y*y + z*z + w*w)`.
This method is faster than [len](#len) but less accurate.

```cpp
float lenApprox()
```

  
**Returns `float`:**

the length of the vector.

----
### normalize
!!! note
    Avoid normalizing a vector with length = `0`.

This method will normalize the vector.
Normalizing a vector will cause it's length to be equal to `1`.
The direction of the vector won't be affected.

```cpp
Vec4& normalize()
```

  
**Returns `Vec4&`:**



----
### normalizeSafe

This method will normalize the vector.
It works almost exactly the same as [normalize](#normalize)
with the difference that zero length vector won't cause any issues.

```cpp
Vec4& normalizeSafe()
```

  
**Returns `Vec4&`:**



----
### normalizeApprox
!!! note
    Avoid normalizing a vector with length = `0`.

This method will normalize the vector with small approximation.
It works almost the same as [normalize](#normalize), but it's faster and less accurate.
It uses the `fast inverse square root` algorithm as the base of calculating square root.

```cpp
Vec4& normalizeApprox()
```

  
**Returns `Vec4&`:**



----
### set

This method will set all of the vector components.

```cpp
void set(float x, float y, float z, float w)
```

**Parameters:**

* `float` **x**: the new vector x component.
* `float` **y**: the new vector y component.
* `float` **z**: the new vector z component.
* `float` **w**: the new vector z component.
  

----
### isEqualEps

This method will return `true` if vectors are approximately the same.
The minimal difference used for comparison is `0.001`.

```cpp
bool isEqualEps(Vec4 vec)
```

**Parameters:**

* `Vec4` **vec**: the vector that will be used in comparision.
  
**Returns `bool`:**

`true` if vectors are approximately the same, otherwise `false`.

----
### abs

This method will return vector with absolute x,y,z,w components.

```cpp
Vec4 abs()
```

  
**Returns `Vec4`:**

the new vector result.

----
### `static` swap

This method will swap x,y,z,w components between two vectors.

```cpp
void swap(Vec4 vec1, Vec4 vec2)
```

**Parameters:**

* `Vec4` **vec1**: the vector that will be used in swap operation.
* `Vec4` **vec2**: the vector that will be used in swap operation.
  

----
### `static` min

This method will return vector with the smallest x,y,z,w components from two vectors.

```cpp
Vec4 min(Vec4 vec1, Vec4 vec2)
```

**Parameters:**

* `Vec4` **vec1**: the vector that will be used in min operation.
* `Vec4` **vec2**: the vector that will be used in min operation.
  
**Returns `Vec4`:**

the new vector result.

----
### `static` max

This method will return vector with the largest x,y,z,w components from two vectors.

```cpp
Vec4 max(Vec4 vec1, Vec4 vec2)
```

**Parameters:**

* `Vec4` **vec1**: the vector that will be used in max operation.
* `Vec4` **vec2**: the vector that will be used in max operation.
  
**Returns `Vec4`:**

the new vector result.

----
### `static` prod

This method will multiply two vectors and return the result as a new vector.

```cpp
Vec4 prod(Vec4 vec1, Vec4 vec2)
```

**Parameters:**

* `Vec4` **vec1**: the vector that will be used in prod operation.
* `Vec4` **vec2**: the vector that will be used in prod operation.
  
**Returns `Vec4`:**

the new vector result.

----
### `static` dot

This method will return the dot product between two vectors.
For normalized vectors dot returns:
- `1` if they point in exactly the same direction
- `-1` if they point in completely opposite directions
and a number in between for other cases (e.g. Dot returns zero if vectors are perpendicular).
For vectors of arbitrary length the Dot return values are similar: they get larger when the angle between vectors decreases.

```cpp
float dot(Vec4 vec1, Vec4 vec2)
```

**Parameters:**

* `Vec4` **vec1**: the vector that will be used in dot operation.
* `Vec4` **vec2**: the vector that will be used in dot operation.
  
**Returns `float`:**

the dot product.

----
### `static` lerp

This method will return interpolated vector between vectors a and b by ratio t.

```cpp
Vec4 lerp(float t, Vec4 v1, Vec4 v2)
```

**Parameters:**

* `float` **t**: interpolation ratio.
* `Vec4` **v1**: the vector used in lerp operation.
* `Vec4` **v2**: the vector used in lerp operation.
  
**Returns `Vec4`:**

the lerp vector result.

----
