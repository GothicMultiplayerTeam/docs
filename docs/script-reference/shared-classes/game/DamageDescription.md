---
title: 'DamageDescription'
---
# `class` DamageDescription <font size="4">(shared-side)</font>
!!! info "Available since version: 0.2.1"

This class represents damage information.


## Properties
### `int` flags 

Represents the damage flags.

----
### `int` damage 

Represents the total damage taken.

----
### `string` item_instance <font size="2">(read-only)</font>
!!! info "Available since version: 0.3.0"
!!! note
    Can be empty if there is no weapon.

Represents the weapon instance used to deal damage.

----
### `int` distance 

Represents the total distance, calculated from origin point to target.

----
### `int` spell_id 
!!! info "Available since version: 0.3.0"

Represents the spell id.

----
### `int` spell_level 
!!! info "Available since version: 0.3.0"

Represents the level of chargeable spells.

----
### `string` node 
!!! info "Available since version: 0.3.0"
!!! note
    Can be empty if there was no projectile.

Represents the name of the node hit by a point projectile.

----

## Methods
No methods.

----
