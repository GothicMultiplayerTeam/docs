---
title: 'disableMusicSystem'
---
# `function` disableMusicSystem <font size="4">(client-side)</font>
!!! info "Available since version: 0.1.9"

This function will toggle ingame music system.  
Disabling the music system will cause to halt all of the ingame music playback.

## Declaration
```cpp
void disableMusicSystem(bool toggle)
```

## Parameters
* `bool` **toggle**: `true` if you want to disable ingame music system, otherwise `false`.
  

