---
title: 'getNpcActions'
---
# `function` getNpcActions <font size="4">(client-side)</font>
!!! info "Available since version: 0.3.0"

This function gets informations about elements in NPC action queue.

## Declaration
```cpp
array[{type, id}]|null getNpcActions(int npc_id)
```

## Parameters
* `int` **npc_id**: the remote npc identifier.
  
## Returns `array[{type, id}]|null`
The array containing information about queue elements, otherwise `null`.

