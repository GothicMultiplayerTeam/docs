---
title: 'unspawnNpc'
---
# `function` unspawnNpc <font size="4">(client-side)</font>
!!! info "Available since version: 0.1.1"
!!! note
    This function won't destroy NPC, only remove it from the current world. In order to permanently delete npc, you have to call ``destroyNpc`` instead.

This function will remove NPC from the current world.

## Declaration
```cpp
bool unspawnNpc(int npcId)
```

## Parameters
* `int` **npcId**: the npc id.
  
## Returns `bool`
`true` when NPC was successfully unspawned, otherwise `false`.

