---
title: 'isNpcHosted'
---
# `function` isNpcHosted <font size="4">(client-side)</font>
!!! info "Available since version: 0.3.0"

This function checks whether given NPC is hosted by local player.

## Declaration
```cpp
bool isNpcHosted(int npcId)
```

## Parameters
* `int` **npcId**: the npc id.
  
## Returns `bool`
`true` when remote NPC is hosted by local player, otherwise `false`.

