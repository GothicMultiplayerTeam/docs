---
title: 'getHostedNpcs'
---
# `function` getHostedNpcs <font size="4">(client-side)</font>
!!! info "Available since version: 0.3.1"

This function is used to retrieve server npcs ids hosted by local player.

## Declaration
```cpp
array[int] getHostedNpcs()
```

## Parameters
No parameters.
  
## Returns `array[int]`
ids of hosted server npcs by local player.

