---
title: 'getStreamedPlayers'
---
# `function` getStreamedPlayers <font size="4">(client-side)</font>
!!! info "Available since version: 0.3.1"

This function is used to retrieve streamed players and npcs for local player.

## Declaration
```cpp
array[int] getStreamedPlayers()
```

## Parameters
No parameters.
  
## Returns `array[int]`
ids of streamed players and npcs by local player.

