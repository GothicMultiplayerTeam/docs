---
title: 'spawnNpc'
---
# `function` spawnNpc <font size="4">(client-side)</font>
!!! info "Available since version: 0.1.1"

This function will add NPC to the current world.

## Declaration
```cpp
bool spawnNpc(int npcId, string instance = "PC_HERO")
```

## Parameters
* `int` **npcId**: the npc id.
* `string` **instance**: the instance of the NPC.
  
## Returns `bool`
`true` when NPC was successfully spawned, otherwise `false`.

