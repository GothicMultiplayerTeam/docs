---
title: 'getNpcLastActionId'
---
# `function` getNpcLastActionId <font size="4">(client-side)</font>
!!! info "Available since version: 0.3.0"

This function gets last action identifier, that was enqued to the NPC action queue.
Every action in queue has associated unique id, by which can be identified.

## Declaration
```cpp
int getNpcLastActionId(int npc_id)
```

## Parameters
* `int` **npc_id**: the remote npc identifier.
  
## Returns `int`
The last finished action identifier, otherwise `-1`.

