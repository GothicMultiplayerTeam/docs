---
title: 'isLocalNpc'
---
# `function` isLocalNpc <font size="4">(client-side)</font>
!!! info "Available since version: 0.3.0"

This function checks whether id related to given object is local NPC.

## Declaration
```cpp
bool isLocalNpc(int npcId)
```

## Parameters
* `int` **npcId**: the npc id.
  
## Returns `bool`
`true` when object is local NPC, otherwise `false`.

