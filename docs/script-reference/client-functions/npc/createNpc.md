---
title: 'createNpc'
---
# `function` createNpc <font size="4">(client-side)</font>
!!! info "Available since version: 0.1.1"
!!! note
    By default NPCs won't be added to world. In order to do that, you have to call [spawnNpc](../spawnNpc).
!!! note
    Npc id will always begins from max slots value.

This function will create NPC.

## Declaration
```cpp
int createNpc(string name)
```

## Parameters
* `string` **name**: the name of the NPC.
  
## Returns `int`
The id of created NPC. If id is set to `-1` then creation of npc failed.

