---
title: 'destroyNpc'
---
# `function` destroyNpc <font size="4">(client-side)</font>
!!! info "Available since version: 0.1.1"

This function will destroy NPC.

## Declaration
```cpp
bool destroyNpc(int npcId)
```

## Parameters
* `int` **npcId**: the NPC id.
  
## Returns `bool`
`true` when NPC was successfully destroyed, otherwise `false`.

