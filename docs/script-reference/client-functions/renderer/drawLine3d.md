---
title: 'drawLine3d'
---
# `function` drawLine3d <font size="4">(client-side)</font>
!!! info "Available since version: 0.3.0"

This function will draw 3d line on the screen using world coordinates.

## Declaration
```cpp
void drawLine3d(float x1, float y1, float z1, float x2, float y2, float z2, int r, int g, int b, bool useDepth = true)
```

## Parameters
* `float` **x1**: the position on X axis of the begin point.
* `float` **y1**: the position on Y axis of the begin point.
* `float` **z1**: the position on Z axis of the begin point.
* `float` **x2**: the position on X axis of the end point.
* `float` **y2**: the position on Y axis of the end point.
* `float` **z2**: the position on Z axis of the end point.
* `int` **r**: the red color component in RGB model.
* `int` **g**: the green color component in RGB model.
* `int` **b**: the blue color component in RGB model.
* `bool` **useDepth**: the flag which indicates if line should be rendered using depth buffer.
  

