---
title: 'drawLine'
---
# `function` drawLine <font size="4">(client-side)</font>
!!! info "Available since version: 0.3.0"

This function will draw 2d line on the screen using pixel coordinates.

## Declaration
```cpp
void drawLine(float x1, float y1, float x2, float y2, int r, int g, int b)
```

## Parameters
* `float` **x1**: the pixel position on X axis of the begin point.
* `float` **y1**: the pixel position on Y axis of the begin point.
* `float` **x2**: the pixel position on X axis of the end point.
* `float` **y2**: the pixel position on Y axis of the end point.
* `int` **r**: the red color component in RGB model.
* `int` **g**: the green color component in RGB model.
* `int` **b**: the blue color component in RGB model.
  

