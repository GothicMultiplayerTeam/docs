---
title: 'getWaypoint'
---
# `function` getWaypoint <font size="4">(client-side)</font>
!!! info "Available since version: 0.1.4"

This function is used to retrieve the position of specified waypoint.

## Declaration
```cpp
Vec3 getWaypoint(string name)
```

## Parameters
* `string` **name**: the name of the waypoint.
  
## Returns `Vec3`
The position of waypoint.

