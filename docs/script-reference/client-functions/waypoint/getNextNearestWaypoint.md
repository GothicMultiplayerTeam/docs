---
title: 'getNextNearestWaypoint'
---
# `function` getNextNearestWaypoint <font size="4">(client-side)</font>
!!! info "Available since version: 0.1.4"

This function is used to retrieve the information about second nearest waypoint from the specified position.

## Declaration
```cpp
{name, x, y, z} getNextNearestWaypoint(int x, int y, int z)
```

## Parameters
* `int` **x**: the position in the world on the x axis.
* `int` **y**: the position in the world on the y axis.
* `int` **z**: the position in the world on the z axis.
  
## Returns `{name, x, y, z}`
Waypoint information.

