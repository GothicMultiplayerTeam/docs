---
title: 'getKeyDelayFirst'
---
# `function` getKeyDelayFirst <font size="4">(client-side)</font>
!!! info "Available since version: 0.3.0"

The function retrieves the initial delay in milliseconds after the first key press while holding down a key.

## Declaration
```cpp
float getKeyDelayFirst(int type)
```

## Parameters
* `int` **type**: the location from which you want to retrieve key delay. For more information see [Key delay constants](../../../client-constants/key-delay/).
  
## Returns `float`
the delay in milliseconds for subsequent key presses.

