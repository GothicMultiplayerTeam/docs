---
title: 'setClipboardText'
---
# `function` setClipboardText <font size="4">(client-side)</font>
!!! info "Available since version: 0.1.4.7"

This function will fill the client clipboard with specified text.

## Declaration
```cpp
void setClipboardText(string text)
```

## Parameters
* `string` **text**: the text you want to fill into client clipboard.
  

