---
title: 'getKeyboardLangName'
---
# `function` getKeyboardLangName <font size="4">(client-side)</font>
!!! info "Available since version: 0.3.0"

The function gets the currently used ISO 639-2 standard keyboard lang name, e.g: `"eng"`.

## Declaration
```cpp
string getKeyboardLangName()
```

## Parameters
No parameters.
  
## Returns `string`
Lang name that is currently used by the keyboard. See [ISO 639-2 codes](https://en.wikipedia.org/wiki/List_of_ISO_639-2_codes) for more details.

