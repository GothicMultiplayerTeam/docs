---
title: 'isControlsDisabled'
---
# `function` isControlsDisabled <font size="4">(client-side)</font>
!!! info "Available since version: 0.0.4"

The function is used to check whether default game actions are disabled.

## Declaration
```cpp
bool isControlsDisabled()
```

## Parameters
No parameters.
  
## Returns `bool`
`true` when default game actions are disabled, otherwise `false`.

