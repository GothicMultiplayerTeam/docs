---
title: 'setKeyDelayFirst'
---
# `function` setKeyDelayFirst <font size="4">(client-side)</font>
!!! info "Available since version: 0.3.0"

The function sets the initial delay in milliseconds after the first key press while holding down a key.

## Declaration
```cpp
void setKeyDelayFirst(float delay)
```

## Parameters
* `float` **delay**: the delay in miliseconds.
  

