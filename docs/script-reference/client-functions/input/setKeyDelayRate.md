---
title: 'setKeyDelayRate'
---
# `function` setKeyDelayRate <font size="4">(client-side)</font>
!!! info "Available since version: 0.3.0"
!!! note
    You can disable auto repeat key in [onKeyDown](../../../client-events/input/onKeyDown/) event by setting the delay to negative value.

The function sets the current delay in miliseconds between key presses while holding key.

## Declaration
```cpp
void setKeyDelayRate(float delay)
```

## Parameters
* `float` **delay**: the delay in miliseconds.
  

