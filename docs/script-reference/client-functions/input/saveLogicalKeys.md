---
title: 'saveLogicalKeys'
---
# `function` saveLogicalKeys <font size="4">(client-side)</font>
!!! info "Available since version: 0.2"

This function saves all of the logical key bindings, closing up the game will save all of the changed controls settings in `gothic.ini`.

## Declaration
```cpp
void saveLogicalKeys()
```

## Parameters
No parameters.
  

