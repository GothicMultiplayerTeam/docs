---
title: 'getGothic1Controls'
---
# `function` getGothic1Controls <font size="4">(client-side)</font>
!!! info "Available since version: 0.2.1"

The function is used to check if gothic 1 controls are enabled.

## Declaration
```cpp
bool getGothic1Controls()
```

## Parameters
No parameters.
  
## Returns `bool`
`true` when gothic 1 controls are enabled, otherwise `false`.

