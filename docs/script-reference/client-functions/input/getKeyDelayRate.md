---
title: 'getKeyDelayRate'
---
# `function` getKeyDelayRate <font size="4">(client-side)</font>
!!! info "Available since version: 0.3.0"

The function retrieves the current delay in miliseconds between key presses while holding key.

## Declaration
```cpp
float getKeyDelayRate(int type)
```

## Parameters
* `int` **type**: the location from which you want to retrieve key delay. For more information see [Key delay constants](../../../client-constants/key-delay/).
  
## Returns `float`
the value in miliseconds after next key press will be emitted.

