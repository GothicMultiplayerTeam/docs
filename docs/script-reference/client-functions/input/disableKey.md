---
title: 'disableKey'
---
# `function` disableKey <font size="4">(client-side)</font>
!!! info "Available since version: 0.0.0"

This function will disable/enable specified keyboard key, like: ESCAPE, TAB, etc.

## Declaration
```cpp
void disableKey(int keyId, bool toggle)
```

## Parameters
* `int` **keyId**: the id of the key. For more information see [Key constants](../../../client-constants/key/).
* `bool` **toggle**: `true` when you want to disable specified keyboard key, otherwise `false`.
  

