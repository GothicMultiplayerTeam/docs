---
title: 'chatInputSetCaretPosition'
---
# `function` chatInputSetCaretPosition <font size="4">(client-side)</font>
!!! info "Available since version: 0.3.0"

This function will set the input caret position on the screen.

## Declaration
```cpp
void chatInputSetCaretPosition(int pos)
```

## Parameters
* `int` **pos**: position on the screen in virtuals.
  

