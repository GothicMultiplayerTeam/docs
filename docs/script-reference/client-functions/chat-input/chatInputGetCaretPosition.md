---
title: 'chatInputGetCaretPosition'
---
# `function` chatInputGetCaretPosition <font size="4">(client-side)</font>
!!! info "Available since version: 0.3.0"

The function is used to retrieve the input caret position on the screen.

## Declaration
```cpp
int chatInputGetCaretPosition()
```

## Parameters
No parameters.
  
## Returns `int`
pos Position of input caret on the screen.

