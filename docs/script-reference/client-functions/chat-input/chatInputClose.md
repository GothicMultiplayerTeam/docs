---
title: 'chatInputClose'
---
# `function` chatInputClose <font size="4">(client-side)</font>
!!! info "Available since version: 0.0.1"

This function will hide the text input from the screen.

## Declaration
```cpp
void chatInputClose()
```

## Parameters
No parameters.
  

