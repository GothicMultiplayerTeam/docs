---
title: 'chatInputSetFont'
---
# `function` chatInputSetFont <font size="4">(client-side)</font>
!!! info "Available since version: 0.1.10"

This function will set the chat input font.

## Declaration
```cpp
void chatInputSetFont(string font)
```

## Parameters
* `string` **font**: that will be set for chat input.
  

