---
title: 'changeWorld'
---
# `function` changeWorld <font size="4">(client-side)</font>
!!! info "Available since version: 0.0.4"

This function will change the current game world.

## Declaration
```cpp
void changeWorld(string world, string startPointName)
```

## Parameters
* `string` **world**: the path to the target world (.ZEN). World path is relative to directory `_Work/Data/Worlds`.
* `string` **startPointName**: ="" the name of the vob to which the player will be moved. If passed empty string, player will be placed at world start point. If vob with specified name doesn't exists or world doesn't have start point, player will be placed at {0, 150, 0} coordinates.
  

