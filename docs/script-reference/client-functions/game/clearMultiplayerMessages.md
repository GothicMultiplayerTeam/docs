---
title: 'clearMultiplayerMessages'
---
# `function` clearMultiplayerMessages <font size="4">(client-side)</font>
!!! info "Available since version: 0.0.0"

This function will clear multiplayer messages shown while joining the server.

## Declaration
```cpp
void clearMultiplayerMessages()
```

## Parameters
No parameters.
  

