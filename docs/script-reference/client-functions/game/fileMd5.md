---
title: 'fileMd5'
---
# `function` fileMd5 <font size="4">(client-side)</font>
!!! info "Available since version: 0.3.5"

This function will try to calculate the MD5 hash of the file content it as hexadecimal text representation.

## Declaration
```cpp
string|null fileMd5(int dirID, string filepath)
```

## Parameters
* `int` **dirID**: the game directory id, for more information see [Dir constants](../../../client-constants/dir/).
* `string` **filepath**: the path to real filename, e.g: `"DEFAULT-C.TEX"`, can contain subdirectories, e.g: `"System/Gothic2.exe"`
  
## Returns `string|null`
Returns string hash as hexadecimal text representation, or `null` in case if file doesn't exists.

