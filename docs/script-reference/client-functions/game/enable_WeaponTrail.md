---
title: 'enable_WeaponTrail'
---
# `function` enable_WeaponTrail <font size="4">(client-side)</font>
!!! info "Available since version: 0.2.0"
!!! note
    By default weapon trails are enabled.

This function will enable/disable melee weapon trails globally.

## Declaration
```cpp
void enable_WeaponTrail(bool toggle)
```

## Parameters
* `bool` **toggle**: `true` when you want to enable the weapon trails, otherwise `false`.
  

