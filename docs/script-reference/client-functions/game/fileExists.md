---
title: 'fileExists'
---
# `function` fileExists <font size="4">(client-side)</font>
!!! info "Available since version: 0.3.5"
!!! note
    relative paths aren't supported due to security concerns.

This function checks if file exists at given file path.

## Declaration
```cpp
bool fileExists(int dirID, string filepath)
```

## Parameters
* `int` **dirID**: the game directory id, for more information see [Dir constants](../../../client-constants/dir/).
* `string` **filepath**: the path to real filename, e.g: `"DEFAULT-C.TEX"`, can contain subdirectories, e.g: `"System/Gothic2.exe"`
  
## Returns `bool`
`true` if file exists at given game location, otherwise `false`.

