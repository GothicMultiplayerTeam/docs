---
title: 'enable_MunitionTrail'
---
# `function` enable_MunitionTrail <font size="4">(client-side)</font>
!!! info "Available since version: 0.2.2"
!!! note
    By default munition trails are enabled.

This function will enable/disable ranged weapon munition trails globally.

## Declaration
```cpp
void enable_MunitionTrail(bool toggle)
```

## Parameters
* `bool` **toggle**: `true` when you want to enable the munition trails, otherwise `false`.
  

