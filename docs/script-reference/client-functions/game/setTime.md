---
title: 'setTime'
---
# `function` setTime <font size="4">(client-side)</font>
!!! info "Available since version: 0.1.0"

This function will set the time in the game to the given time.

## Declaration
```cpp
void setTime(int hour, int min, int day = 0)
```

## Parameters
* `int` **hour**: the hour of new time (in the range between 0-23).
* `int` **min**: the minute of new time (in the range between 0-59).
* `int` **day**: the day of new time.
  

