---
title: 'getLODStrengthOverride'
---
# `function` getLODStrengthOverride <font size="4">(client-side)</font>
!!! info "Available since version: 0.1.10"

This function will get the LOD strength override used by dynamic models (ASC) like armors, npc body, etc...

## Declaration
```cpp
float getLODStrengthOverride()
```

## Parameters
No parameters.
  
## Returns `float`
The LOD strength value. - `-1` use values stored within mesh. - `0` don't use LOD for mesh. - `1` >= use aggressive LOD

