---
title: 'getSightFactor'
---
# `function` getSightFactor <font size="4">(client-side)</font>
!!! info "Available since version: 0.0.4"

This function will get the sight range of the player.

## Declaration
```cpp
float getSightFactor()
```

## Parameters
No parameters.
  
## Returns `float`
The player sight factor (in the range between 0.02-5.0).

