---
title: 'getMultiplayerParams'
---
# `function` getMultiplayerParams <font size="4">(client-side)</font>
!!! info "Available since version: 0.3.0"
!!! note
    `nickname` and `connect` params aren't included.

This function is used to get multiplayer params that has been passed during the launch of the game.

## Declaration
```cpp
table getMultiplayerParams()
```

## Parameters
No parameters.
  
## Returns `table`
params as key value pairs passed during the launch of the game.

