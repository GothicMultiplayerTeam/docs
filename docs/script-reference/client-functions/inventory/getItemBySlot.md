---
title: 'getItemBySlot'
---
# `function` getItemBySlot <font size="4">(client-side)</font>
!!! info "Available since version: 0.0.2"

The function is used to get the item from hero inventory that is on specified slot.

## Declaration
```cpp
Item|null getItemBySlot(int inventorySlot)
```

## Parameters
* `int` **inventorySlot**: the inventory slot.
  
## Returns `Item|null`
the item object. For more information see [Item class](../../../client-classes/game/Item/).

