---
title: 'getCurrentInventorySlot'
---
# `function` getCurrentInventorySlot <font size="4">(client-side)</font>
!!! info "Available since version: 0.1.4"
!!! note
    the returned value will always start from `0`.

The function is used to get currently selected hero inventory slot.

## Declaration
```cpp
int getCurrentInventorySlot()
```

## Parameters
No parameters.
  
## Returns `int`
the currently selected inventory slot.

