---
title: 'hasItem'
---
# `function` hasItem <font size="4">(client-side)</font>
!!! info "Available since version: 0.2.1"

The function is used to retrieve the amount of the item the player/npc currently has.

## Declaration
```cpp
int hasItem(int id, string instance)
```

## Parameters
* `int` **id**: the player id.
* `string` **instance**: the item instance from Daedalus scripts.
  
## Returns `int`
the item amount that player/npc currently has in inventory or `0` if player doesn't have the item.

