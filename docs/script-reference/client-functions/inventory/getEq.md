---
title: 'getEq'
---
# `function` getEq <font size="4">(client-side)</font>
!!! info "Available since version: 0.0.2"

The function is used to get all of the items from hero inventory.

## Declaration
```cpp
[Item...] getEq()
```

## Parameters
No parameters.
  
## Returns `[Item...]`
the array populated with all of the item objects from hero inventory. For more information see [Item class](../../../client-classes/game/Item/).

