---
title: 'clearInventory'
---
# `function` clearInventory <font size="4">(client-side)</font>
!!! info "Available since version: 0.0.4"

This function will remove all of hero items.

## Declaration
```cpp
void clearInventory()
```

## Parameters
No parameters.
  

