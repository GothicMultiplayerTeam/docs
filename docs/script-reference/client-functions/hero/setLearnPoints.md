---
title: 'setLearnPoints'
---
# `function` setLearnPoints <font size="4">(client-side)</font>
!!! info "Available since version: 0.0.4"

This function will set hero learn points, which can be seen in statistics.

## Declaration
```cpp
void setLearnPoints(int learnPoints)
```

## Parameters
* `int` **learnPoints**: the hero learn points.
  

