---
title: 'getHeroStatus'
---
# `function` getHeroStatus <font size="4">(client-side)</font>
!!! info "Available since version: 0.3.0"

This function is used to get the current hero status that is responsible for selecting active music theme (_STD, _THR, _FGT).

## Declaration
```cpp
int getHeroStatus()
```

## Parameters
No parameters.
  
## Returns `int`
status the hero status. For more information see [Hero Status constants](../../../client-constants/hero-status/).

