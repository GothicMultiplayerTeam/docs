---
title: 'getExp'
---
# `function` getExp <font size="4">(client-side)</font>
!!! info "Available since version: 0.0.4"

This function is used to get current hero experience.

## Declaration
```cpp
int getExp()
```

## Parameters
No parameters.
  
## Returns `int`
the current hero experience.

