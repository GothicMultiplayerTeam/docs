---
title: 'setNextLevelExp'
---
# `function` setNextLevelExp <font size="4">(client-side)</font>
!!! info "Available since version: 0.0.4"

This function will set hero experience required for level up, which can be seen in statistics.

## Declaration
```cpp
void setNextLevelExp(int nextLevelExp)
```

## Parameters
* `int` **nextLevelExp**: the hero experience required for level up.
  

