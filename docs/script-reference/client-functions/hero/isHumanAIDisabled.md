---
title: 'isHumanAIDisabled'
---
# `function` isHumanAIDisabled <font size="4">(client-side)</font>
!!! info "Available since version: 0.3.0"

The function is used to check if hero human AI is disabled or not.

## Declaration
```cpp
bool isHumanAIDisabled()
```

## Parameters
No parameters.
  
## Returns `bool`
`true` when hero human AI is disabled, otherwise `false`.

