---
title: 'getNextLevelExp'
---
# `function` getNextLevelExp <font size="4">(client-side)</font>
!!! info "Available since version: 0.0.4"

This function is used to get current hero experience required for level up.

## Declaration
```cpp
int getNextLevelExp()
```

## Parameters
No parameters.
  
## Returns `int`
the current hero experience required for level up.

