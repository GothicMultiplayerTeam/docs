---
title: 'letterHeightPx'
---
# `function` letterHeightPx <font size="4">(client-side)</font>
!!! info "Available since version: 0.1.10"
!!! note
    Every character inside the font will have the same height, because font defines the character height.

This function will get the character height on screen in pixels.

## Declaration
```cpp
int letterHeightPx()
```

## Parameters
No parameters.
  
## Returns `int`
the character height.

