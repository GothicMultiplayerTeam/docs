---
title: 'textWidthPx'
---
# `function` textWidthPx <font size="4">(client-side)</font>
!!! info "Available since version: 0.0.4"

This function will get the text width on screen in pixels.  
If text consists of multiple lines, the width of the longest one will be retuned.

## Declaration
```cpp
int textWidthPx(string text)
```

## Parameters
* `string` **text**: the text of which you want to calculate width.
  
## Returns `int`
the text width.

