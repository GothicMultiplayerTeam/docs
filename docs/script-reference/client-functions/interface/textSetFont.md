---
title: 'textSetFont'
---
# `function` textSetFont <font size="4">(client-side)</font>
!!! info "Available since version: 0.0.4"
!!! note
    Use this function only when you want to calculate text width/height for specified font, and restore the original font after you're done.

This function will set the default screen font.

## Declaration
```cpp
void textSetFont(string font)
```

## Parameters
* `string` **font**: the new screen font, e.g `"FONT_OLD_20_WHITE_HI.TGA"`.
  

