---
title: 'getCursorTxt'
---
# `function` getCursorTxt <font size="4">(client-side)</font>
!!! info "Available since version: 0.0.2"
!!! note
    The default cursor texture is `"LO.TGA"`.

This function will get the mouse cursor texture.

## Declaration
```cpp
string getCursorTxt()
```

## Parameters
No parameters.
  
## Returns `string`
txt the cursor texture.

