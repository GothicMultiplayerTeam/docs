---
title: 'letterWidthPx'
---
# `function` letterWidthPx <font size="4">(client-side)</font>
!!! info "Available since version: 0.0.4"

This function will get the character width on screen in pixels.

## Declaration
```cpp
int letterWidthPx(string character)
```

## Parameters
* `string` **character**: the character of which you want to calculate the width.
  
## Returns `int`
the text width.

