---
title: 'getActiveMenu'
---
# `function` getActiveMenu <font size="4">(client-side)</font>
!!! info "Available since version: 0.2.1"

The function will get the currently used zCMenu name.

## Declaration
```cpp
string getActiveMenu()
```

## Parameters
No parameters.
  
## Returns `string`
the name of the active used game menu, or `""` if there's no active menu.

