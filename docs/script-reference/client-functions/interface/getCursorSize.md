---
title: 'getCursorSize'
---
# `function` getCursorSize <font size="4">(client-side)</font>
!!! info "Available since version: 0.1.10"

This function will get the mouse cursor size on screen in virtuals.

## Declaration
```cpp
Vec2i getCursorSize()
```

## Parameters
No parameters.
  
## Returns `Vec2i`
the cursor size on screen.

