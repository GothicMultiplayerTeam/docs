---
title: 'getBarPosition'
---
# `function` getBarPosition <font size="4">(client-side)</font>
!!! info "Available since version: 0.0.0"

This function is used to retrieve specified status bar position on the screen.

## Declaration
```cpp
{x, y} getBarPosition(int type)
```

## Parameters
* `int` **type**: the type of status bar you want to get the position of. For more information see [HUD constants](../../../client-constants/hud/).
  
## Returns `{x, y}`
Position of specified status bar on the screen.

