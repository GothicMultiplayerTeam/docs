---
title: 'getResolution'
---
# `function` getResolution <font size="4">(client-side)</font>
!!! info "Available since version: 0.1.0"

This function will get the current resolution of the game window.

## Declaration
```cpp
{x, y, bpp} getResolution()
```

## Parameters
No parameters.
  
## Returns `{x, y, bpp}`
Current resolution of game window.

