---
title: 'setPlayerAniFpsRateMultiplier'
---
# `function` setPlayerAniFpsRateMultiplier <font size="4">(client-side)</font>
!!! info "Available since version: 0.3.0.5"
!!! note
    Speed multiplier will be reset on player instance change.

This function is used to set player/npc animation speed multiplier.

## Declaration
```cpp
void setPlayerAniFpsRateMultiplier(int id, string aniName, float speedMultiplier)
```

## Parameters
* `int` **id**: the player id.
* `string` **aniName**: the name of the animation, e.g: `"S_RUNL"`.
* `float` **speedMultiplier**: the animation speed multiplier.
  

