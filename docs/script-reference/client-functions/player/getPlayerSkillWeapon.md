---
title: 'getPlayerSkillWeapon'
---
# `function` getPlayerSkillWeapon <font size="4">(client-side)</font>
!!! info "Available since version: 0.1.1"

This function will get the hero/npc skill weapon.

## Declaration
```cpp
int|null getPlayerSkillWeapon(int id, int skillId)
```

## Parameters
* `int` **id**: the player id.
* `int` **skillId**: . For more information see [Skill weapon constants](../../../shared-constants/skill-weapon/).
  
## Returns `int|null`
the percentage value in range <0, 100> or `null` if player isn't spawned.

