---
title: 'hitPlayer'
---
# `function` hitPlayer <font size="4">(client-side)</font>
!!! info "Available since version: 0.1.1"

This function is used to simulate hit between attacker and victim.  
It will only work with hero/npc. The victim will receive damage calculated damage by the game.

## Declaration
```cpp
bool hitPlayer(int attackerId, int victimId)
```

## Parameters
* `int` **attackerId**: the attacker id.
* `int` **victimId**: the victim id.
  
## Returns `bool`
`true` if hit was successfully simulated, otherwise `false`.

