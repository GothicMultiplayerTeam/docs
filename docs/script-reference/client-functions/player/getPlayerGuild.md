---
title: 'getPlayerGuild'
---
# `function` getPlayerGuild <font size="4">(client-side)</font>
!!! info "Available since version: 0.2.1"

This function will get the player/npc guild.

## Declaration
```cpp
int|null getPlayerGuild(int id)
```

## Parameters
* `int` **id**: the player id.
  
## Returns `int|null`
the guild id or `null` if player isn't spawned.

