---
title: 'isPlayerInWater'
---
# `function` isPlayerInWater <font size="4">(client-side)</font>
!!! info "Available since version: 0.1.10"

The function is used to check whether player/npc is in the water (standing, swimming or diving).

## Declaration
```cpp
bool|null isPlayerInWater(int id)
```

## Parameters
* `int` **id**: the player id.
  
## Returns `bool|null`
`true` when player is in the water, otherwise `false` or `null` if player isn't spawned.

