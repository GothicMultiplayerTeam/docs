---
title: 'setPlayerStrength'
---
# `function` setPlayerStrength <font size="4">(client-side)</font>
!!! info "Available since version: 0.1.1"

This function will set the player/npc strength points.

## Declaration
```cpp
void setPlayerStrength(int id, int strength)
```

## Parameters
* `int` **id**: the player id.
* `int` **strength**: the strength points amount.
  

