---
title: 'getPlayerFaceAnis'
---
# `function` getPlayerFaceAnis <font size="4">(client-side)</font>
!!! info "Available since version: 0.3.0"

This function will get the currently played face animations on player/npc.

## Declaration
```cpp
[{aniName, layer}]|null getPlayerFaceAnis(int id)
```

## Parameters
* `int` **id**: the player id.
  
## Returns `[{aniName, layer}]|null`
the array of objects describing face animation or `null` if player isn't spawned.

