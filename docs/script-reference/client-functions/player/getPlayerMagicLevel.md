---
title: 'getPlayerMagicLevel'
---
# `function` getPlayerMagicLevel <font size="4">(client-side)</font>
!!! danger "Deprecated since version: 0.3.0"

This function will get the player/npc magic level.

## Declaration
```cpp
int|null getPlayerMagicLevel(int id)
```

## Parameters
* `int` **id**: the player id.
  
## Returns `int|null`
the magicLevel or `null` if player isn't spawned.

