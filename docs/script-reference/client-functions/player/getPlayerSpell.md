---
title: 'getPlayerSpell'
---
# `function` getPlayerSpell <font size="4">(client-side)</font>
!!! info "Available since version: 0.1.10"

This function will get the equipped player/npc spell.

## Declaration
```cpp
Item|null getPlayerSpell(int id, int slotId)
```

## Parameters
* `int` **id**: the player id.
* `int` **slotId**: the equipped spell slotId in range <0, 6>.
  
## Returns `Item|null`
the item object or `null`. For more information see [Item class](../../../client-classes/game/Item/).

