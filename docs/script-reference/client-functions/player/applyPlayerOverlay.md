---
title: 'applyPlayerOverlay'
---
# `function` applyPlayerOverlay <font size="4">(client-side)</font>
!!! info "Available since version: 0.2"

This function will apply animation overlay on player/npc.

## Declaration
```cpp
bool applyPlayerOverlay(int id, int overlayId)
```

## Parameters
* `int` **id**: the player id.
* `int` **overlayId**: the overlay id from `mds.xml` file, e.g: `Mds.id("HUMANS_MILITIA.MDS")`
  
## Returns `bool`
`true` if animation overlay was successfully applied on npc, otherwise `false`.

