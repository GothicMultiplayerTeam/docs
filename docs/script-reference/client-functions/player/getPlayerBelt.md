---
title: 'getPlayerBelt'
---
# `function` getPlayerBelt <font size="4">(client-side)</font>
!!! info "Available since version: 0.1.10"

This function will get the equipped player/npc belt.

## Declaration
```cpp
Item|null getPlayerBelt(int id)
```

## Parameters
* `int` **id**: the player id.
  
## Returns `Item|null`
the item object or `null`. For more information see [Item class](../../../client-classes/game/Item/).

