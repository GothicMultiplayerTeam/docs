---
title: 'isPlayerDead'
---
# `function` isPlayerDead <font size="4">(client-side)</font>
!!! info "Available since version: 0.0.1"

The function is used to check whether player/npc is dead.

## Declaration
```cpp
bool|null isPlayerDead(int id)
```

## Parameters
* `int` **id**: the player id.
  
## Returns `bool|null`
`true` when player is dead, otherwise `false` or `null` if player isn't spawned.

