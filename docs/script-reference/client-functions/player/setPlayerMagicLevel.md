---
title: 'setPlayerMagicLevel'
---
# `function` setPlayerMagicLevel <font size="4">(client-side)</font>
!!! danger "Deprecated since version: 0.3.0"

This function will set the player/npc magic level.

## Declaration
```cpp
void setPlayerMagicLevel(int id, int magicLevel)
```

## Parameters
* `int` **id**: the player id.
* `int` **magicLevel**: the magic level.
  

