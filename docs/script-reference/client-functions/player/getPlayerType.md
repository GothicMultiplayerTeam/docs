---
title: 'getPlayerType'
---
# `function` getPlayerType <font size="4">(client-side)</font>
!!! info "Available since version: 0.1.1"

This function will get the player/npc type.

The possible return values are:  
[OBJ_PLAYER](../../../client-constants/objecttype), [OBJ_LOCALPLAYER](../../../client-constants/objecttype), [OBJ_NPC](../../../client-constants/objecttype), [OBJ_LOCALNPC](../../../client-constants/objecttype).

## Declaration
```cpp
int|null getPlayerType(int id)
```

## Parameters
* `int` **id**: the player id.
  
## Returns `int|null`
the player type or `null` if player isn't created.

