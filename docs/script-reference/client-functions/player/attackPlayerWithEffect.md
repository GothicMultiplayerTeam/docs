---
title: 'attackPlayerWithEffect'
---
# `function` attackPlayerWithEffect <font size="4">(client-side)</font>
!!! info "Available since version: 0.1.9"
!!! note
    The victim will receive damage calculated damage by the game.

This function is used to play effect and deal damage from attacker.
It will only work with hero/npc.

## Declaration
```cpp
void attackPlayerWithEffect(int attackerId, int victimId, int damageType, int damage, bool isProjectile, int level, string effect)
```

## Parameters
* `int` **attackerId**: the attacker id.
* `int` **victimId**: the victim id.
* `int` **damageType**: the damage type, can be `0`. For more information [Damage constants](../../../shared-constants/damage/).
* `int` **damage**: the amount of damage that , can be `0`.
* `bool` **isProjectile**: the value which will determine if effect is projectile.
* `int` **level**: the level of the VisualFX.
* `string` **effect**: the name of the VisualFX, e.g `"SPELLFX_TELEPORT"`.
  

