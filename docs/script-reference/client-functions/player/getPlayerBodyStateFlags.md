---
title: 'getPlayerBodyStateFlags'
---
# `function` getPlayerBodyStateFlags <font size="4">(client-side)</font>
!!! info "Available since version: 0.2.1.9"

This function will return player/npc bodystate and it's modifier flags e.g: if player is currently burning or if its transformed.

## Declaration
```cpp
int|null getPlayerBodyStateFlags(int id)
```

## Parameters
* `int` **id**: the player id.
  
## Returns `int|null`
the player bodystate or `null` if player isn't spawned. For more information see [BodyStates](../../../client-constants/bodystate/) and [BodyState Flags](../../../client-constants/bodystateflags/).

