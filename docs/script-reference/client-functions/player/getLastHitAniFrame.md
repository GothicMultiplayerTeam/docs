---
title: 'getLastHitAniFrame'
---
# `function` getLastHitAniFrame <font size="4">(client-side)</font>
!!! info "Available since version: 0.1.4.9"

This function will get the last hit ani frame from player/npc.

## Declaration
```cpp
int|null getLastHitAniFrame(int id)
```

## Parameters
* `int` **id**: the player id.
  
## Returns `int|null`
the last hit ani frame id or `null` if player isn't spawned.

