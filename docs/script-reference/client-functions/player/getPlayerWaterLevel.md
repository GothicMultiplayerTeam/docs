---
title: 'getPlayerWaterLevel'
---
# `function` getPlayerWaterLevel <font size="4">(client-side)</font>
!!! info "Available since version: 0.3.0"

This function will get player/npc water level.

## Declaration
```cpp
int|null getPlayerWaterLevel(int id)
```

## Parameters
* `int` **id**: the player id.
  
## Returns `int|null`
current water level; 0 = outside water, 1 = walking in water, 2 = swimming or `null` if player isn't created.

