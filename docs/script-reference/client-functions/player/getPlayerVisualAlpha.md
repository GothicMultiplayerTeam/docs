---
title: 'getPlayerVisualAlpha'
---
# `function` getPlayerVisualAlpha <font size="4">(client-side)</font>
!!! info "Available since version: 0.1.10"

This function will get player/npc visual alpha.

## Declaration
```cpp
float|null getPlayerVisualAlpha(int id)
```

## Parameters
* `int` **id**: the player id.
  
## Returns `float|null`
the player visual alpha or `null` if player isn't spawned.

