---
title: 'getPlayerVisual'
---
# `function` getPlayerVisual <font size="4">(client-side)</font>
!!! info "Available since version: 0.0.1"

This function will get the player/npc visual.

## Declaration
```cpp
{bodyModel, bodyTxt, headModel, headTxt, teethTxt, skinColor}|null getPlayerVisual(int id)
```

## Parameters
* `int` **id**: the player id.
  
## Returns `{bodyModel, bodyTxt, headModel, headTxt, teethTxt, skinColor}|null`
player visual or `null` if player isn't spawned.

