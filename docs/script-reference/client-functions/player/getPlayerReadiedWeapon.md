---
title: 'getPlayerReadiedWeapon'
---
# `function` getPlayerReadiedWeapon <font size="4">(client-side)</font>
!!! info "Available since version: 0.2"

This function will get player readied (drawn) weapon.

## Declaration
```cpp
Item|null getPlayerReadiedWeapon(int id)
```

## Parameters
* `int` **id**: the player id.
  
## Returns `Item|null`
the item object or `null`. For more information see [Item class](../../../client-classes/game/Item/).

