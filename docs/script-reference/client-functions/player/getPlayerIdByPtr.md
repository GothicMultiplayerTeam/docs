---
title: 'getPlayerIdByPtr'
---
# `function` getPlayerIdByPtr <font size="4">(client-side)</font>
!!! info "Available since version: 0.2.1"

This function will get player id from npc pointer.

## Declaration
```cpp
int getPlayerIdByPtr(userpointer npc)
```

## Parameters
* `userpointer` **npc**: the raw pointer to npc object.
  
## Returns `int`
the player id, if npc isn't managed by the g2o, it will return `-1`.

