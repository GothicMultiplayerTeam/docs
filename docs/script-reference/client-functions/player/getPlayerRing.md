---
title: 'getPlayerRing'
---
# `function` getPlayerRing <font size="4">(client-side)</font>
!!! info "Available since version: 0.1.10"

This function will get the equipped player/npc ring.

## Declaration
```cpp
Item|null getPlayerRing(int id, int handId)
```

## Parameters
* `int` **id**: the player id.
* `int` **handId**: the handId. For more information see [Hand constants](../../../shared-constants/hand/).
  
## Returns `Item|null`
the item object or `null`. For more information see [Item class](../../../client-classes/game/Item/).

