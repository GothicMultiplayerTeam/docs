---
title: 'stopAni'
---
# `function` stopAni <font size="4">(client-side)</font>
!!! info "Available since version: 0.1.0"

This function is used to stop played animation on player/npc.

## Declaration
```cpp
void stopAni(int id, string aniName = "" the name of the animation that you want to stop. The default value is `")
```

## Parameters
* `int` **id**: the player id.
* `string` **aniName**: "` which means that the first active ani will be stopped.
  

