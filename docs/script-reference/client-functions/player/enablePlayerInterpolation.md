---
title: 'enablePlayerInterpolation'
---
# `function` enablePlayerInterpolation <font size="4">(client-side)</font>
!!! info "Available since version: 0.3.0"

This function is used enable/disable player interpolation.

## Declaration
```cpp
void enablePlayerInterpolation(int id, bool toggle)
```

## Parameters
* `int` **id**: the player id.
* `bool` **toggle**: toggles player interpolation state.
  

