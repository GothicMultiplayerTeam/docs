---
title: 'getPlayerAni'
---
# `function` getPlayerAni <font size="4">(client-side)</font>
!!! info "Available since version: 0.0.1"

This function will get the active player/npc animation.

## Declaration
```cpp
string|null getPlayerAni(int id)
```

## Parameters
* `int` **id**: the player id.
  
## Returns `string|null`
the ani name, e.g: `"S_RUN"` or `null` if player isn't spawned.

