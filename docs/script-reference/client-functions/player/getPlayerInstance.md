---
title: 'getPlayerInstance'
---
# `function` getPlayerInstance <font size="4">(client-side)</font>
!!! info "Available since version: 0.1.0"

This function will get the player/npc instance.

## Declaration
```cpp
string|null getPlayerInstance(int id)
```

## Parameters
* `int` **id**: the player id.
  
## Returns `string|null`
the instance or `null` if player isn't created.

