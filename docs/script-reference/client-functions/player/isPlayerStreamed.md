---
title: 'isPlayerStreamed'
---
# `function` isPlayerStreamed <font size="4">(client-side)</font>
!!! info "Available since version: 0.0.1"

The function is used to check if player or npc is inside streamed area.  
Only players that are currently in streamed area, can be visible to your hero in game.  
Streamed area is basicly, a roller with some radius and height, and elements inside are controlled by server.

## Declaration
```cpp
bool|null isPlayerStreamed(int id)
```

## Parameters
* `int` **id**: the player id.
  
## Returns `bool|null`
`true` when player or npc is streamed, otherwise `false` or `null` if player isn't created.

