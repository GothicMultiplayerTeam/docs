---
title: 'getPlayerBodyState'
---
# `function` getPlayerBodyState <font size="4">(client-side)</font>
!!! info "Available since version: 0.2.1.9"

This function will return player/npc bodystate.

## Declaration
```cpp
int|null getPlayerBodyState(int id)
```

## Parameters
* `int` **id**: the player id.
  
## Returns `int|null`
the player bodystate or `null` if player isn't spawned. For more information see [BodyStates](../../../client-constants/bodystate/).

