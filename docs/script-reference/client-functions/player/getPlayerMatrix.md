---
title: 'getPlayerMatrix'
---
# `function` getPlayerMatrix <font size="4">(client-side)</font>
!!! info "Available since version: 0.2"

This function will get player matrix reference.

## Declaration
```cpp
Mat4|null getPlayerMatrix(int id)
```

## Parameters
* `int` **id**: the player id.
  
## Returns `Mat4|null`
the player matrix reference or `null` if player isn't spawned.

