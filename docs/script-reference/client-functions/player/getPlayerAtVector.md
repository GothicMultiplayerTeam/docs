---
title: 'getPlayerAtVector'
---
# `function` getPlayerAtVector <font size="4">(client-side)</font>
!!! info "Available since version: 0.2"

This function will get player at vector.

## Declaration
```cpp
Vec3|null getPlayerAtVector(int id)
```

## Parameters
* `int` **id**: the player id.
  
## Returns `Vec3|null`
the player at vector or `null` if player isn't spawned.

