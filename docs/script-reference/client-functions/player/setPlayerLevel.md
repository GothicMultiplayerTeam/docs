---
title: 'setPlayerLevel'
---
# `function` setPlayerLevel <font size="4">(client-side)</font>
!!! info "Available since version: 0.2.1"

This function will set the player/npc level.

## Declaration
```cpp
void setPlayerLevel(int id, int level)
```

## Parameters
* `int` **id**: the player id.
* `int` **level**: the level.
  

