---
title: 'getPlayerRangedWeapon'
---
# `function` getPlayerRangedWeapon <font size="4">(client-side)</font>
!!! info "Available since version: 0.0.1"

This function will get the equipped player/npc ranged weapon.

## Declaration
```cpp
Item|null getPlayerRangedWeapon(int id)
```

## Parameters
* `int` **id**: the player id.
  
## Returns `Item|null`
the item object or `null`. For more information see [Item class](../../../client-classes/game/Item/).

