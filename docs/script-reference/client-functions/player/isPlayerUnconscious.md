---
title: 'isPlayerUnconscious'
---
# `function` isPlayerUnconscious <font size="4">(client-side)</font>
!!! info "Available since version: 0.0.1"

The function is used to check whether player/npc is in unconscious state.  
The player/npc will be unconscious, when it gets beaten up, but not killed.

## Declaration
```cpp
bool|null isPlayerUnconscious(int id)
```

## Parameters
* `int` **id**: the player id.
  
## Returns `bool|null`
`true` when player is unconscious, otherwise `false` or `null` if player isn't spawned.

