---
title: 'getPlayerCollision'
---
# `function` getPlayerCollision <font size="4">(client-side)</font>
!!! info "Available since version: 0.0.1"

This function is used to check player/npc dynamic collision detection is enabled.

## Declaration
```cpp
bool|null getPlayerCollision(int id)
```

## Parameters
* `int` **id**: the player id.
  
## Returns `bool|null`
toggle `true` when player has dynamic collision detection enabled, otherwise `false` or `null` if player isn't spawned.

