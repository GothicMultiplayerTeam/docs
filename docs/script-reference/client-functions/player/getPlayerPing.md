---
title: 'getPlayerPing'
---
# `function` getPlayerPing <font size="4">(client-side)</font>
!!! info "Available since version: 0.0.1"
!!! note
    The function can return `-1` if player isn't connected, or when you call it on npc.

This function will get the player ping.  
Ping gets updated after each 2500 miliseconds.

## Declaration
```cpp
int|null getPlayerPing(int id)
```

## Parameters
* `int` **id**: the player id.
  
## Returns `int|null`
the current player ping or `null` if player isn't created.

