---
title: 'getActFrame'
---
# `function` getActFrame <font size="4">(client-side)</font>
!!! info "Available since version: 0.1.4.9"

This function will get the active ani frame of played animation on player/npc.

## Declaration
```cpp
int|null getActFrame(int id)
```

## Parameters
* `int` **id**: the player id.
  
## Returns `int|null`
the active frame of played animation on player from aniChannel 0, `-1` if no animation is being played or `null` if player isn't spawned.

