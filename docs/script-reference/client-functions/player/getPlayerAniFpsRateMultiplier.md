---
title: 'getPlayerAniFpsRateMultiplier'
---
# `function` getPlayerAniFpsRateMultiplier <font size="4">(client-side)</font>
!!! info "Available since version: 0.3.0.5"

This function is used to get player/npc animation speed multiplier.

## Declaration
```cpp
float|null getPlayerAniFpsRateMultiplier(int id, string aniName)
```

## Parameters
* `int` **id**: the player id.
* `string` **aniName**: the name of the animation, e.g: `"S_RUNL"`.
  
## Returns `float|null`
the current animation speed multiplier or `null` if player isn't spawned or given animation doesn't exists.

