---
title: 'setPlayerTalent'
---
# `function` setPlayerTalent <font size="4">(client-side)</font>
!!! info "Available since version: 0.2"

This function will toggle the player/npc talent.

## Declaration
```cpp
void setPlayerTalent(int id, int talentId, int talentValue)
```

## Parameters
* `int` **id**: the npc id.
* `int` **talentId**: the talent id. For more information see [Talent constants](../../../shared-constants/talent/).
* `int` **talentValue**: the new talent value.
  

