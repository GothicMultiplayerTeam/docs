---
title: 'setPlayerName'
---
# `function` setPlayerName <font size="4">(client-side)</font>
!!! info "Available since version: 0.0.1"

This function will set the player/npc nickname for client.

## Declaration
```cpp
void setPlayerName(int id, string name)
```

## Parameters
* `int` **id**: the player id.
* `string` **name**: the playe/npc name.
  

