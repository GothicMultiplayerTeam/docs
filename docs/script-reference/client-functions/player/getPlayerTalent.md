---
title: 'getPlayerTalent'
---
# `function` getPlayerTalent <font size="4">(client-side)</font>
!!! info "Available since version: 0.2"

This function will get the player/npc talent.

## Declaration
```cpp
int|null getPlayerTalent(int id, int talentId)
```

## Parameters
* `int` **id**: the player id.
* `int` **talentId**: the talent id. For more information see [Talent constants](../../../shared-constants/talent/).
  
## Returns `int|null`
the current talent value for specific talent id or `null` if player isn't spawned.

