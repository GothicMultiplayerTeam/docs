---
title: 'setPlayerVisual'
---
# `function` setPlayerVisual <font size="4">(client-side)</font>
!!! info "Available since version: 0.0.1"

This function will set the player/npc visual.

## Declaration
```cpp
void setPlayerVisual(int id, string bodyModel, int bodyTxt, string headModel, int headTxt, int teethTxt = 0, int skinColor = 0)
```

## Parameters
* `int` **id**: the player id.
* `string` **bodyModel**: the name of the body model (ASC), e.g: `HUM_BODY_NAKED0`.
* `int` **bodyTxt**: the numeric id of body texture file. Texture id can be read from `V(number)` filename part, for example, in this file: `HUM_BODY_NAKED_V8_C0-C.TEX` id is 8.
* `string` **headModel**: the name of the head model (MMS), e.g: `HUM_HEAD_PONY`.
* `int` **headTxt**: the numeric id of head texture file. Texture id can be read from `V(number)` filename part, for example, in this file: `HUM_HEAD_V18_C0-C.TEX` id is 18.
* `int` **teethTxt**: the numeric id of teeth texture file. Texture id can be read from `V(number)` filename part, for example, in this file: `HUM_TEETH_V2-C.TEX` id is 2.
* `int` **skinColor**: the color variant of head & body texture file. Color variant can be read from `C(number)` filename part, for example, in this file: `HUM_BODY_NAKED_V1_C3-C.TEX` color variant is 3.
  

