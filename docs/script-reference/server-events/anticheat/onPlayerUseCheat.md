---
title: 'onPlayerUseCheat'
---
# `event` onPlayerUseCheat <font size="4">(server-side)</font>
!!! info "Available since version: 0.2"
!!! note
    Detecting some type of forbidden tools may take, even a few minutes. Server need time to analyze player data.

This event is triggered when player uses some of forbidden cheat tools.

## Parameters
```c++
int playerid, int type
```

* `int` **playerid**: the id of the player who used some type of trainer/cheat.
* `int` **type**: the type of used trainer/cheat. For more information see [AntiCheat constants](../../../server-constants/anticheat/).

