---
title: 'onUnban'
---
# `event` onUnban <font size="4">(server-side)</font>
!!! info "Available since version: 0.3.0"
!!! tip "This event can be canceled"
!!! note
    If serial/mac/ip/name indexes doesn't exist, then the parameters has not been specified when ban was added.

This event is triggered when ban with specified info is being removed.

## Parameters
```c++
table ban
```

* `table` **ban**: the ban info.

