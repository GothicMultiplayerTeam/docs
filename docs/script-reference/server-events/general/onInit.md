---
title: 'onInit'
---
# `event` onInit <font size="4">(server-side)</font>
!!! info "Available since version: 0.0.1"

This event is triggered when server successfully starts up.

## Parameters
No parameters.

