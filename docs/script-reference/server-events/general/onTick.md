---
title: 'onTick'
---
# `event` onTick <font size="4">(server-side)</font>
!!! info "Available since version: 0.1.10"

This event is triggered in every server main loop iteration.

## Parameters
No parameters.

