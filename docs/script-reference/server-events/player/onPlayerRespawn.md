---
title: 'onPlayerRespawn'
---
# `event` onPlayerRespawn <font size="4">(server-side)</font>
!!! info "Available since version: 0.0.1"

This event is triggered when a player respawns after death.

## Parameters
```c++
int playerid
```

* `int` **playerid**: the id of the player who respawned after death.

