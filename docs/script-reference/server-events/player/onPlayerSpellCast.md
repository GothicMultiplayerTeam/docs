---
title: 'onPlayerSpellCast'
---
# `event` onPlayerSpellCast <font size="4">(server-side)</font>
!!! info "Available since version: 0.1.3"
!!! tip "This event can be canceled"
!!! note
    Right now transformation and summon spells are not supported, despite this event will be triggered for them. Cancelling this event willl prevent this action to be synced to other players.

This event is triggered when player is casting some spell.

## Parameters
```c++
int playerid, string|null instance, int spellLevel
```

* `int` **playerid**: the id of the player who casts the spell.
* `string|null` **instance**: the item instance from Daedalus scripts.
* `int` **spellLevel**: the level of charged spell

