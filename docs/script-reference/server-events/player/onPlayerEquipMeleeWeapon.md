---
title: 'onPlayerEquipMeleeWeapon'
---
# `event` onPlayerEquipMeleeWeapon <font size="4">(server-side)</font>
!!! info "Available since version: 0.0.1"

This event is triggered when player equips or unequips melee weapon. When item is unequiped, `null` is returned instead.

## Parameters
```c++
int playerid, string|null instance
```

* `int` **playerid**: the id of the player who equips an melee weapon.
* `string|null` **instance**: the item instance from Daedalus scripts.

