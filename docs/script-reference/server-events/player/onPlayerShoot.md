---
title: 'onPlayerShoot'
---
# `event` onPlayerShoot <font size="4">(server-side)</font>
!!! info "Available since version: 0.2.1"

This event is triggered when player shoot using ranged weapon.

## Parameters
```c++
int playerid, string|null munition
```

* `int` **playerid**: the id of the player who just shot.
* `string|null` **munition**: the item instance from Daedalus scripts.

