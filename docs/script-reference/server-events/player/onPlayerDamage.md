---
title: 'onPlayerDamage'
---
# `event` onPlayerDamage <font size="4">(server-side)</font>
!!! info "Available since version: 0.3.5"
!!! tip "This event can be canceled"

This event is triggered when one player hits another player.

## Parameters
```c++
int playerid, int killerid, DamageDescription description
```

* `int` **playerid**: the id of the player who was hit.
* `int` **killerid**: the id of the killer. If killerid is set to `-1`, it means that there was no killer. In this particular case damage source can be fall from a tall object or scripts.
* `DamageDescription` **description**: a structure containing damage information. For more information see [DamageDescription](../../../shared-classes/game/DamageDescription/)

