---
title: 'onPlayerChangeWorld'
---
# `event` onPlayerChangeWorld <font size="4">(server-side)</font>
!!! info "Available since version: 0.0.1"
!!! tip "This event can be canceled"

This event is triggered when player tries to change his currently played world (**ZEN**).

## Parameters
```c++
int playerid, string world, string waypoint
```

* `int` **playerid**: the id of the player who tries to change the played world.
* `string` **world**: a filename name of the world.
* `string` **waypoint**: a name of the waypoint that the player will be teleported to.

