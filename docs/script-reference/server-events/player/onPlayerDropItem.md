---
title: 'onPlayerDropItem'
---
# `event` onPlayerDropItem <font size="4">(server-side)</font>
!!! info "Available since version: 0.1.0"
!!! tip "This event can be canceled"
!!! note
    Cancelling this event will delete the dropped item from the world.

This event is triggered when player drops an item from his inventory to the ground.

## Parameters
```c++
int playerid, ItemGround itemGround
```

* `int` **playerid**: the id of the player who tries to drop the item on the ground.
* `ItemGround` **itemGround**: the ground item object which represents the dropped item by the player.

