---
title: 'onPlayerTeleport'
---
# `event` onPlayerTeleport <font size="4">(server-side)</font>
!!! info "Available since version: 0.2"

This event is triggered when player gets teleported by the game to the specified vob.

## Parameters
```c++
int playerid, string vobName
```

* `int` **playerid**: the id of the player who gets teleported by the game.
* `string` **vobName**: represents the name of the vob that player gets teleported to.

