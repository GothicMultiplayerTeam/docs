---
title: 'onPlayerDead'
---
# `event` onPlayerDead <font size="4">(server-side)</font>
!!! info "Available since version: 0.0.1"

This event is triggered when one player kills another player.

## Parameters
```c++
int playerid, int killerid
```

* `int` **playerid**: the id of the player who died.
* `int` **killerid**: the id of the player who killed other player. If killerid is set to `-1`, it means that there was no killer.

