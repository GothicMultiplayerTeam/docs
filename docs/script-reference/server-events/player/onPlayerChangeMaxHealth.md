---
title: 'onPlayerChangeMaxHealth'
---
# `event` onPlayerChangeMaxHealth <font size="4">(server-side)</font>
!!! info "Available since version: 0.0.1"

This event is triggered when player maximum health changes.

## Parameters
```c++
int playerid, int oldMaxHP, int newMaxHP
```

* `int` **playerid**: the id of the player whose maxium health points gets changed.
* `int` **oldMaxHP**: the previous maximum health points of the player.
* `int` **newMaxHP**: the new maximum health points of the player.

