---
title: 'onPlayerMessage'
---
# `event` onPlayerMessage <font size="4">(server-side)</font>
!!! info "Available since version: 0.0.1"

This event is triggered when a player types the message on the chat.

## Parameters
```c++
int playerid, string message
```

* `int` **playerid**: the id of the player who typed the message.
* `string` **message**: the message typed by the player.

