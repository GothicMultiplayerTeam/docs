---
title: 'onNpcDestroyed'
---
# `event` onNpcDestroyed <font size="4">(server-side)</font>
!!! info "Available since version: 0.3.0"

This event is triggered when remote NPC is destroyed.

## Parameters
```c++
int npc_id
```

* `int` **npc_id**: the id of the destroyed remote npc.

