---
title: 'onNpcActionSent'
---
# `event` onNpcActionSent <font size="4">(server-side)</font>
!!! info "Available since version: 0.3.0"

This event is triggered when server sends NPC action to streamed players.

## Parameters
```c++
int npc_id, int action_type, int action_id
```

* `int` **npc_id**: the npc identifier.
* `int` **action_type**: the action type.
* `int` **action_id**: the unique action identifier.

