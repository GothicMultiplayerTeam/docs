---
title: 'onNpcChangeHostPlayer'
---
# `event` onNpcChangeHostPlayer <font size="4">(server-side)</font>
!!! info "Available since version: 0.3.0"

This event is triggered when NPC host is changed.
Every remote NPC is hosted by one of spawned players in order to get valid position of NPC.

## Parameters
```c++
int npc_id, int current_id, int previous_id
```

* `int` **npc_id**: the id of the remote npc.
* `int` **current_id**: the id of the current host, can be -1 if there is no current host.
* `int` **previous_id**: the id of the previous host, can be -1 if there was no previous host.

