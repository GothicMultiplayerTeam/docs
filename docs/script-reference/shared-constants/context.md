---
title: 'Context'
---
# `constants` Context <font size="4">(shared-side)</font>

| Name         | Description                          |
| :----------: | :----------------------------------- |
| `DAMAGE_CTX` | Represents damage script context.<br/>Damage context extends packet, that sent information about hit. |
| `EQUIPMENT_CTX` | Represents equipment script context.<br/>Equipment context extends packet, that sent information about equip/unequip/use item. |
