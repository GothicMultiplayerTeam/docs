---
title: 'Weapon mode'
---
# `constants` Weapon mode <font size="4">(shared-side)</font>

| Name         | Description                          |
| :----------: | :----------------------------------- |
| `WEAPONMODE_NONE` | Represents npc none weapon mode. |
| `WEAPONMODE_FIST` | Represents npc fist weapon mode. |
| `WEAPONMODE_DAG` | Represents npc dagger weapon mode. |
| `WEAPONMODE_1HS` | Represents npc one handed weapon mode (melee weapon). |
| `WEAPONMODE_2HS` | Represents npc two handed weapon mode (melee weapon). |
| `WEAPONMODE_BOW` | Represents npc bow weapon mode (ranged weapon). |
| `WEAPONMODE_CBOW` | Represents npc crossbow weapon mode (ranged weapon). |
| `WEAPONMODE_MAG` | Represents npc magic weapon mode (spell). |
| `WEAPONMODE_MAX` | Represents npc maximum weapon mode (not actuall weapon mode, weapon modes count). |
