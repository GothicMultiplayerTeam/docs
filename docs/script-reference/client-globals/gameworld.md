---
title: 'GameWorld'
---
# `World&` GameWorld <font size="4">(client-side)</font>

Represents the zengine game world instance.  
For more information see [World](../../client-classes/game/World/).