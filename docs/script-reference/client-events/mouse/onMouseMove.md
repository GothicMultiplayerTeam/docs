---
title: 'onMouseMove'
---
# `event` onMouseMove <font size="4">(client-side)</font>
!!! info "Available since version: 0.2"

This event triggers whenever the user moves the mouse.

## Parameters
```c++
int x, int y
```

* `int` **x**: this value represents direction in which mouse moves on horizontal axis. If value is negative, then user is moving mouse left, otherwise it means that mouse gets moved right.
* `int` **y**: this value represents direction in which mouse moves on vertical axis. If value is negative, then user is moving mouse up, otherwise it means that mouse gets moved down.

