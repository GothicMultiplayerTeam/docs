---
title: 'onNpcChangeHost'
---
# `event` onNpcChangeHost <font size="4">(client-side)</font>
!!! info "Available since version: 0.3.0"

This event is triggered when client receives new NPC action.

## Parameters
```c++
int npc_id, bool toggle
```

* `int` **npc_id**: the remote npc identifier.
* `bool` **toggle**: `true` if npc becomes hosted by local player, otherwise `false`.

