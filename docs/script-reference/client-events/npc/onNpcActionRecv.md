---
title: 'onNpcActionRecv'
---
# `event` onNpcActionRecv <font size="4">(client-side)</font>
!!! info "Available since version: 0.3.0"

This event is triggered when client receives new NPC action.

## Parameters
```c++
int npc_id, int action_type, int action_id
```

* `int` **npc_id**: the remote npc identifier.
* `int` **action_type**: the action type.
* `int` **action_id**: the unique action identifier.

