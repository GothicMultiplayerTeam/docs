---
title: 'onPlayerSpellCast'
---
# `event` onPlayerSpellCast <font size="4">(client-side)</font>
!!! info "Available since version: 0.2.1"
!!! tip "This event can be canceled"
!!! note
    Right now transformation and summon spells are not supported, despite this event will be triggered for them.
!!! note
    Event can be cancelled only for hero and local npcs.

This event is triggered when player is casting a spell.

## Parameters
```c++
int playerid, Item item, int spellLevel
```

* `int` **playerid**: the id of the player whose casted spell.
* `Item` **item**: the item object. For more information see [Item class](../../../client-classes/game/Item/).
* `int` **spellLevel**: the level of charged spell

