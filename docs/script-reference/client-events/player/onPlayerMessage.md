---
title: 'onPlayerMessage'
---
# `event` onPlayerMessage <font size="4">(client-side)</font>
!!! info "Available since version: 0.0.1"

This event is triggered when message is send from specific player or server to the client.

## Parameters
```c++
int id, int r, int g, int b, string message
```

* `int` **id**: the id of the player who sent the message. If id is set to `-1`, then server sent message.
* `int` **r**: the amount of red in the message color `(0 - 255)`.
* `int` **g**: the amount of green in the message color `(0 - 255)`.
* `int` **b**: the amount of blue in the message color `(0 - 255)`.
* `string` **message**: a message sended from server or other player.

