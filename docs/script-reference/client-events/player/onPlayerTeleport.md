---
title: 'onPlayerTeleport'
---
# `event` onPlayerTeleport <font size="4">(client-side)</font>
!!! info "Available since version: 0.2"

This event is triggered when a hero/npc gets teleported by the game.

## Parameters
```c++
int id, string vobName
```

* `int` **id**: the id of the player who was teleported.
* `string` **vobName**: the name of the vob to which the player was teleported.

