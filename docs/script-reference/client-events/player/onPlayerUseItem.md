---
title: 'onPlayerUseItem'
---
# `event` onPlayerUseItem <font size="4">(client-side)</font>
!!! info "Available since version: 0.2"

This event is triggered when hero/npc uses, interacts, opens or consumes any item.

## Parameters
```c++
int id, Item item, int from, int to
```

* `int` **id**: the id of the player who uses item.
* `Item` **item**: the item object. For more information see [Item class](../../../client-classes/game/Item/).
* `int` **from**: represents previous state for interact item.
* `int` **to**: represents current state for interact item.

