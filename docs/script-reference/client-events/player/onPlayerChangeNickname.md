---
title: 'onPlayerChangeNickname'
---
# `event` onPlayerChangeNickname <font size="4">(client-side)</font>
!!! info "Available since version: 0.0.4"

This event is triggered when player changed his nickname.

## Parameters
```c++
int id, string nickname
```

* `int` **id**: the id of the player who changed his nickname.
* `string` **nickname**: a new nickname of the player.

