---
title: 'onPlayerUnspawn'
---
# `event` onPlayerUnspawn <font size="4">(client-side)</font>
!!! info "Available since version: 0.0.1"

This event is triggered when player is unspawned.
Player can be unspawned by leaving stream range or by scripts and only from server side.

## Parameters
```c++
int id
```

* `int` **id**: the id of spawned player.

