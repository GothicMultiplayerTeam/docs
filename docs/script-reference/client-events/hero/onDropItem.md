---
title: 'onDropItem'
---
# `event` onDropItem <font size="4">(client-side)</font>
!!! info "Available since version: 0.3.0"
!!! tip "This event can be canceled"

This event is triggered when hero drops an item from his inventory to the ground.

## Parameters
```c++
Item item
```

* `Item` **item**: object representing game item. For more information see [Item class](../../../client-classes/game/Item/).

