---
title: 'onAnim'
---
# `event` onAnim <font size="4">(client-side)</font>
!!! info "Available since version: 0.1.0"
!!! tip "This event can be canceled"

This event is triggered when played hero animation id is changed.

## Parameters
```c++
string ani
```

* `string` **ani**: the name of played animation by the hero.

