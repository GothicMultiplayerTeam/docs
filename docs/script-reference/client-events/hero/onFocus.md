---
title: 'onFocus'
---
# `event` onFocus <font size="4">(client-side)</font>
!!! info "Available since version: 0.0.2"

This event is triggered when hero change focused player or npc.

## Parameters
```c++
int currentId, int previousId
```

* `int` **currentId**: the id of currently focused player or npc. It can be `-1` when hero lost focus or focused other object than player or npc.
* `int` **previousId**: the id of previously focused player or npc. It can be `-1` if previous focus target wasn't set or was not a player or npc.

