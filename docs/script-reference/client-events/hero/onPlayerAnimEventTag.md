---
title: 'onPlayerAnimEventTag'
---
# `event` onPlayerAnimEventTag <font size="4">(client-side)</font>
!!! info "Available since version: 0.3.0"

This event is triggered when current frame of player animation have eventTag added.

## Parameters
```c++
int playerid, string eventTag, string param1, string param2
```

* `int` **playerid**: the id of the hero/npc.
* `string` **eventTag**: the name of eventTag.
* `string` **param1**: the first event param.
* `string` **param2**: the second event param.

