---
title: 'onLostFocus'
---
# `event` onLostFocus <font size="4">(client-side)</font>
!!! info "Available since version: 0.1.0"

This event is triggered when hero loses focus on object like player, npc or vob.

## Parameters
```c++
int type, int id, string name
```

* `int` **type**: the type of currently focused vob. For more information see [Vob types](../../../client-constants/vob/).
* `int` **id**: the id of currently focused vob. If focused target is not player or npc this value is `-1`.
* `string` **name**: focused target displayed name.

