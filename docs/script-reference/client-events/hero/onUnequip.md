---
title: 'onUnequip'
---
# `event` onUnequip <font size="4">(client-side)</font>
!!! info "Available since version: 0.0.1"
!!! tip "This event can be canceled"

This event is triggered when hero is unequipping item.

## Parameters
```c++
Item item
```

* `Item` **item**: the item object. For more information see [Item class](../../../client-classes/game/Item/).

