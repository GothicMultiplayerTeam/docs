---
title: 'onSink'
---
# `event` onSink <font size="4">(client-side)</font>
!!! info "Available since version: 0.0.1"
!!! tip "This event can be canceled"

This event is triggered every time when hero is damage caused by drowning.

## Parameters
No parameters.

