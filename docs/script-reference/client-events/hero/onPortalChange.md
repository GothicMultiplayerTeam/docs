---
title: 'onPortalChange'
---
# `event` onPortalChange <font size="4">(client-side)</font>
!!! info "Available since version: 0.3.0"

This event is triggered when hero is changing the portal on the ZEN in which he's currently located.

## Parameters
```c++
string oldSectorName, string newSectorName
```

* `string` **oldSectorName**: the name of portal in which the hero was before the change.
* `string` **newSectorName**: the name of portal in which the hero is currently located.

