---
title: 'onMusicVolumeChange'
---
# `event` onMusicVolumeChange <font size="4">(client-side)</font>
!!! info "Available since version: 0.3.0"

This event is triggered when music volume changes.

## Parameters
```c++
float from, float to
```

* `float` **from**: the previous volume.
* `float` **to**: the new volume.

