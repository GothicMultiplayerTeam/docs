---
title: 'onItemGroundCreate'
---
# `event` onItemGroundCreate <font size="4">(client-side)</font>
!!! info "Available since version: 0.3.0"

This event is triggered when server item ground gets created for the client.

## Parameters
```c++
ItemGround item
```

* `ItemGround` **item**: object representing item on the ground.

