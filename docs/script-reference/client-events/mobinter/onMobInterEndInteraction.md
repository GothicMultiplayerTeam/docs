---
title: 'onMobInterEndInteraction'
---
# `event` onMobInterEndInteraction <font size="4">(client-side)</font>
!!! info "Available since version: 0.3.0.6"
!!! tip "This event can be canceled"

This event is triggered when hero/npc tries ends interaction with mob inter object in the world.
In Gothic, mob inters are special vobs on the map, that hero/npc can interact with. For example bed, door, chest etc.

## Parameters
```c++
int playerid, userdata address, int type
```

* `int` **playerid**: the id of the hero/npc that is interacting with interaction mob.
* `userdata` **address**: the address of interaction mob.
* `int` **type**: the type of interaction mob. For more information see [Vob types](../../../client-constants/vob/).

