---
title: 'onRender'
---
# `event` onRender <font size="4">(client-side)</font>
!!! info "Available since version: 0.0.1"

This event is triggered every game frame.

## Parameters
No parameters.

