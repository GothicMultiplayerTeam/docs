---
title: 'onInit'
---
# `event` onInit <font size="4">(client-side)</font>
!!! info "Available since version: 0.0.1"

This event is triggered when scripts are initialized.  
It will be called before world gets loaded.

## Parameters
No parameters.

