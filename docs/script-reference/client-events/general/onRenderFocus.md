---
title: 'onRenderFocus'
---
# `event` onRenderFocus <font size="4">(client-side)</font>
!!! info "Available since version: 0.0.1"
!!! tip "This event can be canceled"

This event is triggered every game frame, but only when user is focusing other player, npc or vob.

## Parameters
```c++
int type, int id, int x, int y, string name
```

* `int` **type**: the type of currently focused vob. For more information see [Vob types](../../../client-constants/vob/).
* `int` **id**: the id of currently focused vob. If focused target is not player or npc this value is `-1`.
* `int` **x**: screen virtual x position. Position X is centered related to vob.
* `int` **y**: screen virtual y position. Position Y is offsetted above vob.
* `string` **name**: focused target displayed name.

