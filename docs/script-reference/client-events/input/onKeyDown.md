---
title: 'onKeyDown'
---
# `event` onKeyDown <font size="4">(client-side)</font>
!!! info "Available since version: 0.3.0"
!!! tip "This event can be canceled"

This event is triggered when user presses any button on their keyboard, mouse, or joystick.

## Parameters
```c++
int key
```

* `int` **key**: pressed key by user. See [Key constants](../../../client-constants/key/), [Mouse constants](../../../client-constants/mouse/) for more details.

