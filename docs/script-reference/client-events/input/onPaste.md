---
title: 'onPaste'
---
# `event` onPaste <font size="4">(client-side)</font>
!!! info "Available since version: 0.1.4"
!!! tip "This event can be canceled"

This event is triggered when user presses CTRL+V combination.  
Cancelling this event will prevent the text pasting into chat input.

## Parameters
```c++
string text
```

* `string` **text**: a copied text from clipboard.

