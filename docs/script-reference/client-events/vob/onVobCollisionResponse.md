---
title: 'onVobCollisionResponse'
---
# `event` onVobCollisionResponse <font size="4">(client-side)</font>
!!! info "Available since version: 0.3.0"
!!! tip "This event can be canceled"

This event is triggered when collision occurs on the vob. The collision might occur between other vobs or world mesh.

## Parameters
```c++
zarray& collReportList
```

* `zarray&` **collReportList**: the array containing collision reports. For more information see [CollisionReport](../../../client-classes/game/CollisionReport/).

