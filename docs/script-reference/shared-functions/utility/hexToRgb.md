---
title: 'hexToRgb'
---
# `function` hexToRgb <font size="4">(shared-side)</font>
!!! info "Available since version: 0.0.1"

This function will convert hex color to rgb representation.

## Declaration
```cpp
Color hexToRgb(string hex)
```

## Parameters
* `string` **hex**: hexadecimal color representation.
  
## Returns `Color`
The converted rgb color.

