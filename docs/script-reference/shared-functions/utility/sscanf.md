---
title: 'sscanf'
---
# `function` sscanf <font size="4">(shared-side)</font>
!!! info "Available since version: 0.0.1"

The function splits the specified text, interprets it according to format and stores the results into array.

## Declaration
```cpp
array|null sscanf(string format, string text)
```

## Parameters
* `string` **format**: the format by which the text will be splitted. Each letter in the format represents one argument in the text. If the number of letters in the format will be greater than the number of arguments in the text, then function will fail. Currently supported data formats: `d`: integer value. Example 123532. `f`: float value. Example 56.2353. `s`: string. Example "Gothic 2 Online :)".
* `string` **text**: the text which will be splitted into parts.
  
## Returns `array|null`
The array which contains splitted text, or `null` if text couldn't be converted.

