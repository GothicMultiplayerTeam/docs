---
title: 'getMaxSlots'
---
# `function` getMaxSlots <font size="4">(shared-side)</font>
!!! info "Available since version: 0.0.0"

This function will get the max number of slots available on the server.

## Declaration
```cpp
int getMaxSlots()
```

## Parameters
No parameters.
  
## Returns `int`
Max slots number on the server.

