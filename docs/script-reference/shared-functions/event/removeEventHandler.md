---
title: 'removeEventHandler'
---
# `function` removeEventHandler <font size="4">(shared-side)</font>
!!! info "Available since version: 0.1.10"

This function will unbind function from specified event.

## Declaration
```cpp
bool removeEventHandler(string eventName, function func)
```

## Parameters
* `string` **eventName**: the name of the event.
* `function` **func**: the reference to a function which is currently bound to specified event.
  
## Returns `bool`
`true` if the function was bound with specified event and was sucessfully, otherwise `false`.

