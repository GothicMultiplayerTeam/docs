---
title: 'removeEvent'
---
# `function` removeEvent <font size="4">(shared-side)</font>
!!! info "Available since version: 0.0.1"

This function will unregister an event with specified name.

## Declaration
```cpp
void removeEvent(string eventName)
```

## Parameters
* `string` **eventName**: the name of the event.
  

