---
title: 'toggleEvent'
---
# `function` toggleEvent <font size="4">(shared-side)</font>
!!! info "Available since version: 0.3.0"
!!! note
    By default every event is toggled `on` (enabled).

This function will toggle event (enable or disable it globally).
By toggling event off, you can completely disable certain event from calling it's handlers.

## Declaration
```cpp
void toggleEvent(string eventName, bool toggle)
```

## Parameters
* `string` **eventName**: the name of the event.
* `bool` **toggle**: `false` if you want to disable the event, otherwise `true`.
  

