---
title: 'getTimerInterval'
---
# `function` getTimerInterval <font size="4">(shared-side)</font>
!!! info "Available since version: 0.0.1"

This function will get the timer interval.

## Declaration
```cpp
int getTimerInterval(int timerId)
```

## Parameters
* `int` **timerId**: the timer id.
  
## Returns `int`
Returns the timer interval.

