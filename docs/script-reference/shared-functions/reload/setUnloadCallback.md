---
title: 'setUnloadCallback'
---
# `function` setUnloadCallback <font size="4">(shared-side)</font>
!!! info "Available since version: 0.3.0.5"

This function will register a script specific callback that gets invoked when old script gets unloaded during hotreload procedure.  
Depending on which script calls this function the callback will be bound to it.

## Declaration
```cpp
void setUnloadCallback(function callback)
```

## Parameters
* `function` **callback**: the callback that will be executed.
  

