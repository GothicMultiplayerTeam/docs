---
title: 'setReloadCallback'
---
# `function` setReloadCallback <font size="4">(shared-side)</font>
!!! info "Available since version: 0.3.0.5"

This function will register a script specific callback that gets invoked when new script gets loaded during hotreload procedure.
Depending on which script calls this function the callback will be bound to it.

## Declaration
```cpp
void setReloadCallback(function callback)
```

## Parameters
* `function` **callback**: the callback that will be executed.
  

