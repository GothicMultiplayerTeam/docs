---
title: 'Game'
---
# `constants` Game <font size="4">(client-side)</font>

| Name         | Description                          |
| :----------: | :----------------------------------- |
| `GAME_VERSION` | Represents game version. |
