---
title: 'BloodMode'
---
# `constants` BloodMode <font size="4">(client-side)</font>

| Name         | Description                          |
| :----------: | :----------------------------------- |
| `BLOOD_MODE_NONE` | Represents no blood mode. |
| `BLOOD_MODE_PARTICLES` | Represents particles blood mode. |
| `BLOOD_MODE_DECALS` | Represents decals blood mode. |
| `BLOOD_MODE_TRAILS` | Represents trails blood mode. |
| `BLOOD_MODE_AMPLIFICATION` | Represents amplification blood mode. |
