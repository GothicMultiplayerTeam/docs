---
title: 'Key delay'
---
# `constants` Key delay <font size="4">(client-side)</font>

| Name         | Description                          |
| :----------: | :----------------------------------- |
| `KEY_DELAY_GAME` | Represents key delay value from the game. |
| `KEY_DELAY_SYSTEM` | Represents key delay value from the system. |
