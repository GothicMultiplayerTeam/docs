---
title: 'Attack'
---
# `constants` Attack <font size="4">(client-side)</font>

| Name         | Description                          |
| :----------: | :----------------------------------- |
| `ATTACK_FRONT` | Represents front attack (fist mode) |
| `ATTACK_SWORD_LEFT` | Represents left attack (melee weapon) |
| `ATTACK_SWORD_RIGHT` | Represents right attack (melee weapon) |
| `ATTACK_SWORD_FRONT` | Represents front attack (melee weapon) |
