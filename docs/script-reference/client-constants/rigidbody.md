---
title: 'RigidBody'
---
# `constants` RigidBody <font size="4">(client-side)</font>

| Name         | Description                          |
| :----------: | :----------------------------------- |
| `RIGIDBODY_MODE_FLY` | Represents (default) flying mode for rigid body. |
| `RIGIDBODY_MODE_SLIDE` | Represents sliding mode for rigid body. |
