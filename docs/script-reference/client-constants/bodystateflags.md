---
title: 'BodyStateFlags'
---
# `constants` BodyStateFlags <font size="4">(client-side)</font>

| Name         | Description                          |
| :----------: | :----------------------------------- |
| `BS_MOD_HIDDEN` | Represents unknown bodystate flag (UNUSED). |
| `BS_MOD_DRUNK` | Represents bodystate flag indicating that player is drunk (UNUSED). |
| `BS_MOD_NUTS` | Represents unknown bodystate flag (UNUSED). |
| `BS_MOD_BURNING` | Represents bodystate flag indicating that player is burning. |
| `BS_MOD_CONTROLLED` | Represents bodystate flag indicating that player is being controlled (UNUSED). |
| `BS_MOD_TRANSFORMED` | Represents bodystate flag indicating that player is transformed. |
| `BS_MOD_CONTROLLING` | Represents bodystate flag indicating that player is controlling someone (UNUSED). |
| `BS_FLAG_INTERRUPTABLE` | Represents bodystate flag indicating that player bodystate is interruptable. |
| `BS_FLAG_FREEHANDS` | Represents bodystate flag indicating that player hands are free. |
