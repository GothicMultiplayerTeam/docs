---
title: 'Vob'
---
# `constants` Vob <font size="4">(client-side)</font>

| Name         | Description                          |
| :----------: | :----------------------------------- |
| `VOB_ITEM` | Represents oCItem vob type (virtual table). |
| `VOB_MOB` | Represents oCMob vob type (virtual table). |
| `VOB_MOB_BED` | Represents oCMobBed vob type (virtual table). |
| `VOB_MOB_CONTAINER` | Represents oCMobContainer vob type (virtual table). |
| `VOB_MOB_DOOR` | Represents oCMobDoor vob type (virtual table). |
| `VOB_MOB_FIRE` | Represents oCMobFire vob type (virtual table). |
| `VOB_MOB_INTER` | Represents oCMobInter vob type (virtual table). |
| `VOB_MOB_ITEMSLOT` | Represents oCMobItemSlot vob type (virtual table). |
| `VOB_MOB_LADDER` | Represents oCMobLadder vob type (virtual table). |
| `VOB_MOB_LOCKABLE` | Represents oCMobLockable vob type (virtual table). |
| `VOB_MOB_SWITCH` | Represents oCMobSwitch vob type (virtual table). |
| `VOB_MOB_WHEEL` | Represents oCMobWheel vob type (virtual table). |
| `VOB_TRIGGER_BASE` | Represents zCTriggerBase vob type (virtual table). |
| `VOB_CODEMASTER` | Represents zCCodeMaster vob type (virtual table). |
| `VOB_MESSAGEFILTER` | Represents zCMessageFilter vob type (virtual table). |
| `VOB_MOVER_CONTROLER` | Represents zCMoverControler vob type (virtual table). |
| `VOB_TRIGGER_UNTOUCH` | Represents zCTriggerUntouch vob type (virtual table). |
| `VOB_TRIGGER_WORLD_START` | Represents zCTriggerWorldStart vob type (virtual table). |
| `VOB_TRIGGER` | Represents zCTrigger vob type (virtual table). |
| `VOB_CUTSCENE_TRIGGER` | Represents oCCSTrigger vob type (virtual table). |
| `VOB_TRIGGER_CHANGE_LEVEL` | Represents zCTriggerChangeLevel vob type (virtual table). |
| `VOB_TRIGGER_SCRIPT` | Represents zCTriggerScript vob type (virtual table). |
| `VOB_MOVER` | Represents zCMover vob type (virtual table). |
| `VOB_TRIGGER_LIST` | Represents zCTriggerList vob type (virtual table). |
| `VOB_TRIGGER_TELEPORT` | Represents zCTriggerTeleport vob type (virtual table). |
| `VOB_NPC` | Represents oCNpc vob type (virtual table). |
| `VOB_GAME` | Represents oCVob vob type (virtual table). |
| `VOB_ENGINE` | Represents zCVob vob type (virtual table). |
| `VOB_LIGHT` | Represents zCVobLight vob type (virtual table). |
| `VOB_SOUND` | Represents zCVobSound vob type (virtual table). |
| `VOB_START_POINT` | Represents zCVobStartpoint vob type (virtual table). |
| `VOB_WAY_POINT` | Represents zCVobWaypoint vob type (virtual table). |
| `VOB_ERROR` | Represents Error vob type (virtual table). |
