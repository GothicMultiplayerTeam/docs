---
title: 'Dir'
---
# `constants` Dir <font size="4">(client-side)</font>

| Name         | Description                          |
| :----------: | :----------------------------------- |
| `DIR_SYSTEM` | Represents `"\SYSTEM\"` game directory. |
| `DIR_WEB` | Represents `"\WEB\"` game directory. |
| `DIR_SAVEGAMES` | Represents `"\SAVES\"` game directory. |
| `DIR_DATA` | Represents `"\_WORK\DATA\"` game directory. |
| `DIR_ANIMS` | Represents `"\_WORK\DATA\ANIMS\"` game directory. |
| `DIR_CUTSCENES` | Represents `"\_WORK\DATA\CUTSCENES\"` game directory. |
| `DIR_OUTPUTUNITS` | Represents `"\_WORK\DATA\SCRIPTS\CONTENT\CUTSCENE\"` game directory. |
| `DIR_MESHES` | Represents `"\_WORK\DATA\MESHES\"` game directory. |
| `DIR_SCRIPTS` | Represents `"\_WORK\DATA\SCRIPTS\"` game directory. |
| `DIR_SOUND` | Represents `"\_WORK\DATA\SOUND\"` game directory. |
| `DIR_VIDEO` | Represents `"\_WORK\DATA\VIDEO\"` game directory. |
| `DIR_MUSIC` | Represents `"\_WORK\DATA\MUSIC\"` game directory. |
| `DIR_TEX` | Represents `"\_WORK\DATA\TEXTURES\"` game directory. |
| `DIR_TEX_DESKTOP` | Represents `"\_WORK\DATA\TEXTURES\DESKTOP\"` game directory. |
| `DIR_WORLD` | Represents `"\_WORK\DATA\WORLDS\"` game directory. |
| `DIR_PRESETS` | Represents `"\_WORK\DATA\PRESETS\"` game directory. |
| `DIR_TOOLS_DATA` | Represents `"\_WORK\TOOLS\DATA\"` game directory. |
| `DIR_COMPILED_ANIMS` | Represents `"\_WORK\DATA\ANIMS\_COMPILED\"` game directory. |
| `DIR_COMPILED_MESHES` | Represents `"\_WORK\DATA\MESHES\_COMPILED\"` game directory. |
| `DIR_COMPILED_SCRIPTS` | Represents `"\_WORK\DATA\SCRIPTS\_COMPILED\"` game directory. |
| `DIR_COMPILED_TEXTURES` | Represents `"\_WORK\DATA\TEXTURES\_COMPILED\"` game directory. |
| `DIR_TOOLS_CONFIG` | Represents `"\_WORK\TOOLS\"` game directory. |
| `DIR_ROOT` | Represents root game directory, e.g: `"C:\\Program Files (x86)\\JoWood\\Gothic 2"`. |
