---
title: 'TriggerList Process'
---
# `constants` TriggerList Process <font size="4">(client-side)</font>

| Name         | Description                          |
| :----------: | :----------------------------------- |
| `TRIGGERLIST_PROCESS_ALL` | Represents TriggerList process mode that will invoke events on all targets in the list. |
| `TRIGGERLIST_PROCESS_NEXT` | Represents TriggerList process mode that will invoke event on the next target in list. |
| `TRIGGERLIST_PROCESS_RAND` | Represents TriggerList process mode that will invoke event on random target in list. |
