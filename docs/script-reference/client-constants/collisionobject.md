---
title: 'CollisionObject'
---
# `constants` CollisionObject <font size="4">(client-side)</font>

| Name         | Description                          |
| :----------: | :----------------------------------- |
| `COLLISION_OBJECT_UNDEF` | Represents undefined object collision class. |
| `COLLISION_OBJECT_LEVEL_POLYS` | Represents level polys object collision class. |
| `COLLISION_OBJECT_CHARACTER` | Represents character object collision class. |
| `COLLISION_OBJECT_POINT` | Represents point object collision class. |
| `COLLISION_OBJECT_PROJECTILE` | Represents projectile object collision class. |
| `COLLISION_OBJECT_COMPLEX` | Represents complex object collision class. |
