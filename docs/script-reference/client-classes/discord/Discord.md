---
title: 'Discord'
---
# `static class` Discord <font size="4">(client-side)</font>
!!! info "Available since version: 0.2.0"

This class represents some parts of Discord Game SDK.


## Properties
### `DiscordActivity` activity 

Holds activity information.

----

## Methods
### recreate

This method will re-create Discord Game SDK for new application id.

```cpp
bool recreate(string app_id)
```

**Parameters:**

* `string` **app_id**: the application identifier.
  
**Returns `bool`:**

`true` if re-initialization was successful, otherwise `false`.

----
