---
title: 'DiscordActivity'
---
# `class` DiscordActivity <font size="4">(client-side)</font>
!!! info "Available since version: 0.2.0"

This class represents Discord activity manager.


## Properties
### `string` details 
!!! note
    Maximum length of text is 128 characters!

Represents information about what player currently doing.

----
### `string` state 
!!! note
    Maximum length of text is 128 characters!

Represents information about current player state.

----
### `DiscordActivityTimestamps` timestamps 

Holds activity timestamps.

----
### `DiscordActivityAssets` assets 

Holds activity assets.

----
### `DiscordActivityParty` party 

Holds activity party information.

----

## Methods
### update

This method will update current activity displayed information.

```cpp
void update()
```

  

----
