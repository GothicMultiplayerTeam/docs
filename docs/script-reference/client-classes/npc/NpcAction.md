---
title: 'NpcAction'
---
# `class` NpcAction <font size="4">(client-side)</font>
!!! info "Available since version: 0.3.0"

This class represents script NPC action.


## Properties
No properties.

----

## Methods
### getName

This method gets action name for debug purposes.

```cpp
string getName()
```

  
**Returns `string`:**

returns action name.

----
### getResult

This method is triggered after action was finished and initialized successfully.
Used to determine action result.

```cpp
bool getResult(int npc_id)
```

**Parameters:**

* `int` **npc_id**: the remote npc identifier.
  
**Returns `bool`:**

`true` if action was finished, otherwise `false` to continue.

----
### onInit

This method is triggered when NPC action is being initialized.
Used to determine if action was initialized successfully.

```cpp
bool onInit(int npc_id)
```

**Parameters:**

* `int` **npc_id**: the remote npc identifier.
  
**Returns `bool`:**

`true` if action was initialized successfully, otherwise `false`.

----
### onDeinit

This method is triggered when NPC action is being de-initialized.
De-initialization happends when:
- Method `onInit` fails. 
- Actions queue is cleared.
- NPC is being unspawned.

```cpp
void onDeinit(int npc_id)
```

**Parameters:**

* `int` **npc_id**: the remote npc identifier.
  

----
### onUpdate

This method is triggered every frame.
Used to execute action logic.

```cpp
bool onUpdate(int npc_id)
```

**Parameters:**

* `int` **npc_id**: the remote npc identifier.
  
**Returns `bool`:**

`true` if action was finished, otherwise `false` to continue.

----
### `static` onDeserialize

This method is triggered after client receives NPC action.
Used to deserialize data from packet.

```cpp
instance onDeserialize(instance packet)
```

**Parameters:**

* `instance` **packet**: the packet containing information about action.
  
**Returns `instance`:**

Created instance of NPC action, otherwise `null` for failure.

----
