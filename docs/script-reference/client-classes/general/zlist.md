---
title: 'zlist'
---
# `class` zlist <font size="4">(client-side)</font>
!!! info "Available since version: 0.2.0"

This class represents zCList/zCListSort classes from Gothic API.


## Properties
No properties.

----

## Methods
### len

This method will number of elements inside list.

```cpp
int len()
```

  
**Returns `int`:**

the size of the sequence.

----
### isIn

This method will checks whether element is inside list.

```cpp
bool isIn(any element)
```

**Parameters:**

* `any` **element**: the element.
  
**Returns `bool`:**

`true` if element is inside list, otherwise `false`.

----
### insert

This method will insert element to list.

```cpp
void insert(any element)
```

**Parameters:**

* `any` **element**: the element.
  

----
### remove

This method will remove element from list if exists.

```cpp
void remove(any element)
```

**Parameters:**

* `any` **element**: the element.
  

----
