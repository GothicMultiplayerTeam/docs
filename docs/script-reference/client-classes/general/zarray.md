---
title: 'zarray'
---
# `class` zarray <font size="4">(client-side)</font>
!!! info "Available since version: 0.2.1"

This class represents zCArrray class from Gothic API.


## Properties
No properties.

----

## Methods
### len

This method will number of elements inside array.

```cpp
int len()
```

  
**Returns `int`:**

the size of the sequence.

----
### push

This method will push element at the end of the array.

```cpp
void push(any element)
```

**Parameters:**

* `any` **element**: the element.
  

----
### pop

This method will pop element last element from the array.

```cpp
void pop(any element)
```

**Parameters:**

* `any` **element**: the element.
  

----
### insert

This method will insert element to array.

```cpp
void insert(any element)
```

**Parameters:**

* `any` **element**: the element.
  

----
### remove

This method will remove element from array at specified index.

```cpp
void remove(int index)
```

**Parameters:**

* `int` **index**: the element.
  

----
### find

This method will search for given element inside array and return it's index if found or `-1`.

```cpp
int find(any element)
```

**Parameters:**

* `any` **element**: the element.
  
**Returns `int`:**

the found element index, or `-1` if element isn't present in the array.

----
### clear

This method will remove all elements from the array.

```cpp
void clear()
```

  

----
