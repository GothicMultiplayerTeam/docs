---
title: 'Console'
---
# `class` Console <font size="4">(client-side)</font>
!!! info "Available since version: 0.3.0"

This class represents game world.


## Properties
No properties.

----

## Methods
### print

This method will print text to game console.

```cpp
void print(string text, Color color)
```

**Parameters:**

* `string` **text**: text to print.
* `Color` **color**: the text color.
  

----
