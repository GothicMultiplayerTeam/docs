---
title: 'MobLockable'
---
# `class` MobLockable `extends` [MobInter](../MobInter/) <font size="4">(client-side)</font>
!!! info "Available since version: 0.1.0"

This class represents 3d object in the world, which can be locked (i.e. DOORS, CHESTS).

### Constructor
```cpp
MobLockable(string model)
```

**Parameters:**

* `string` **model**: the model name to be used as visual.
### Constructor
```cpp
MobLockable(userpointer ptr)
```

**Parameters:**

* `userpointer` **ptr**: the pointer to the object from the game.

## Properties
### `bool` locked 

Represents the locked state of lockable mob.

----
### `string` keyInstance 
!!! info "Available since version: 0.1.10"

Represents the instance of the key item needed to open this mob.

----
### `string` pickLockStr 
!!! info "Available since version: 0.1.10"

Represents the picklock combination neeeded to open the lock of this mob.  
An example combination looks like that: `LRLRLR`, where `L` - Left and `R` - Right are short's for the direction of the picklock movement, needed to open the lock.

----

## Methods
No methods.

----
