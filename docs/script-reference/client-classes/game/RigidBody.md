---
title: 'RigidBody'
---
# `class` RigidBody <font size="4">(client-side)</font>
!!! info "Available since version: 0.3.0"

This class represents physics simulator.


## Properties
### `float` mass 

Represents the mass of the rigid body.

----
### `float` massInv <font size="2">(read-only)</font>
!!! note
    The value for this field gets calculated each time you set a `mass` field.

Represents the inverse of mass of the rigid body.

----
### `Vec3` position 

Represents the current position of the rigid body.

----
### `Vec3` velocity 

Represents the current velocity of the rigid body.

----
### `Vec3` impulse <font size="2">(read-only)</font>
!!! note
    The value for this field gets calculated each time you set a [velocity](#velocity) field, or call [applyImpulse](#applyImpulse).

Represents the current impulse (linear velocity) of the rigid body.

----
### `Vec3` force <font size="2">(read-only)</font>

Represents the current force of the rigid body.

----
### `float` gravityScale 

Represents the gravity scale ratio that will be used while applying gravity on rigid body.

----
### `Vec3` slideDir <font size="2">(read-only)</font>

Represents the currently used slide direction for rigid body.

----
### `float` slideAngle <font size="2">(read-only)</font>

Represents the currently used slide angle for rigid body.

----
### `bool` gravityOn 

Represents the field that controls whether or not gravity should affect the rigid body.

----
### `bool` collisionHad <font size="2">(read-only)</font>

Represents if collision had occured on rigid body.

----
### `int` mode <font size="2">(read-only)</font>

Represents the currently used physics mode by the rigid body. For more information see [RigidBody Constants](../../../client-constants/rigidbody/).

----

## Methods
### applyForce

This method applies a additional force to a rigid body.

```cpp
void applyForce(Vec3 force)
```

**Parameters:**

* `Vec3` **force**: the force direction.
  

----
### applyImpulse

This method applies a additional impulse to a rigid body.

```cpp
void applyImpulse(Vec3 impulse)
```

**Parameters:**

* `Vec3` **impulse**: the impulse direction.
  

----
