---
title: 'Mob'
---
# `class` Mob `extends` [Vob](../Vob/) <font size="4">(client-side)</font>
!!! info "Available since version: 0.1.0"

This class represents 3d object in the world, which can be focused.

### Constructor
```cpp
Mob(string model)
```

**Parameters:**

* `string` **model**: the model name to be used as visual.
### Constructor
```cpp
Mob(userpointer ptr)
```

**Parameters:**

* `userpointer` **ptr**: the pointer to the object from the game.

## Properties
### `string` name 

Represents the name of the mob, which will be shown when the object is focused.

----
### `string` scemeName <font size="2">(read-only)</font>
!!! info "Available since version: 0.1.10"

Represents the possible state animation, which will be played by player on interaction with mob.

----
### `bool` focusOverride 
!!! info "Available since version: 0.1.10"

Represents the mob focus feature.
When focusOverride is enabled, the mob can be focused in ranged fight mode.

----

## Methods
No methods.

----
