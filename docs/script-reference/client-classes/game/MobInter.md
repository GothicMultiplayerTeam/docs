---
title: 'MobInter'
---
# `class` MobInter `extends` [Mob](../Mob/) <font size="4">(client-side)</font>
!!! info "Available since version: 0.1.0"

This class represents 3d object in the world, which can be focused and interacted with.

### Constructor
```cpp
MobInter(string model)
```

**Parameters:**

* `string` **model**: the model name to be used as visual.
### Constructor
```cpp
MobInter(userpointer ptr)
```

**Parameters:**

* `userpointer` **ptr**: the pointer to the object from the game.

## Properties
### `string` triggerTarget 
!!! info "Available since version: 0.2.1"

Represents the vob name that will be triggered after interacting with the object.

----
### `string` useWithItem 
!!! info "Available since version: 0.2.1"

Represents the required item for the interaction with an object.

----
### `string` sceme 
!!! info "Available since version: 0.2.1"

Represents the sceme part of state animation, which will be played by player on interaction with mob.

----
### `string` conditionFunc 
!!! info "Available since version: 0.3.0"

Represents the Daedalus function name, which will be called when the player tries to interact with the mob.

----
### `int` state 
!!! info "Available since version: 0.2.1"

Represents the current interact state.

----
### `int` stateNum <font size="2">(read-only)</font>
!!! info "Available since version: 0.2.1"

Represents the number of interact states for the object.

----
### `int` stateTarget 
!!! info "Available since version: 0.2.1"

Represents the next target interact state id for the object.

----
### `int` mobStateAni <font size="2">(read-only)</font>
!!! info "Available since version: 0.2.1"

Represents the current ani id used by the object during interaction.

----
### `int` npcStateAni <font size="2">(read-only)</font>
!!! info "Available since version: 0.2.1"

Represents the current ani id used by the npc during interaction with an object.

----
### `bool` rewind 
!!! info "Available since version: 0.2.1"

Represents the rewind feature, if it's set to `true` the object animation will rewind to the beginning state after npc finished interacting with it.

----
### `int` direction 
!!! info "Available since version: 0.2.1"

Represents the next interact state direction for the object. For more information see [MobInter Direction](../../../client-constants/mobinterdirection/)

----

## Methods
No methods.

----
