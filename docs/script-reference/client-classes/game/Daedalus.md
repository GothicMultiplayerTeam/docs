---
title: 'Daedalus'
---
# `static class` Daedalus <font size="4">(client-side)</font>
!!! info "Available since version: 0.2"

This class represents Daedalus scripting interface.


## Properties
### `string` parser 
!!! info "Available since version: 0.3.5"

Represents the used scripting parser, can be one of the following: `gothic`|`sfx`|`particlefx`|`visualfx`|`camera`|`menu`|`music`.

----

## Methods
### symbol
!!! info "Available since version: 0.2.1"

This method will get the daedalus symbol by its id.

```cpp
DaedalusSymbol|null symbol(int id)
```

**Parameters:**

* `int` **id**: the id of the daedalus symbol.
  
**Returns `DaedalusSymbol|null`:**

the daedalus symbol or null.

----
### index
!!! info "Available since version: 0.3.0"

This method will get the daedalus symbol index by its name.

```cpp
int index(string name)
```

**Parameters:**

* `string` **name**: the name of the daedalus symbol.
  
**Returns `int`:**

the daedalus symbol index number.

----
### symbol
!!! info "Available since version: 0.2.1"

This method will get the daedalus symbol by its name.

```cpp
DaedalusSymbol|null symbol(string name)
```

**Parameters:**

* `string` **name**: the name of the daedalus symbol.
  
**Returns `DaedalusSymbol|null`:**

the daedalus symbol or null.

----
### prototype

This method will get the all of the daedalus prototype variables.

```cpp
table prototype(string prototypename)
```

**Parameters:**

* `string` **prototypename**: the name of the daedalus prototype.
  
**Returns `table`:**

the object containing all of the daedalus prototype variables.

----
### instance

This method will get the all of the daedalus instance variables.

```cpp
table instance(string instanceName)
```

**Parameters:**

* `string` **instanceName**: the name of the daedalus instance.
  
**Returns `table`:**

the object containing all of the daedalus instance variables.

----
### call
!!! info "Available since version: 0.3.0"

This method will call the daedalus function by it's unique name.

```cpp
any call(string funcName, ... args)
```

**Parameters:**

* `string` **funcName**: the name of the daedalus function.
* `...` **args**: the additional optional parameters, that will be passed to each function call.
  
**Returns `any`:**

the value returned by the function.

----
### call
!!! info "Available since version: 0.3.0"

This method will call the daedalus function by it's unique idx.

```cpp
any call(int funcIdx, ... args)
```

**Parameters:**

* `int` **funcIdx**: the index of the daedalus function.
* `...` **args**: the additional optional parameters, that will be passed to each function call.
  
**Returns `any`:**

the value returned by the function.

----
