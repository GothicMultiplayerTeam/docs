---
title: 'World'
---
# `class` World <font size="4">(client-side)</font>
!!! info "Available since version: 0.2.0"

This class represents game world.


## Properties
### `string` fileName 

Represents full name of world file (with extension), placed into `_Work/Data/Worlds`.

----
### `string` name 

Represents name of world.

----
### `const zlistsort&` vobs 

Represents list of vobs available in selected world.

----
### `bool` showWaynet 

Enables debug information about waynet in selected world.

----
### `bool` showZonesDebugInfo 

Enables debug information about zones in selected world.

----

## Methods
### loadVobTree
!!! info "Available since version: 0.3.5"

This method will load vobtree from a `_Work/Data/Worlds` directory and add it to current world.

```cpp
userdata loadVobTree(string fileName)
```

**Parameters:**

* `string` **fileName**: the name of the vobtree file, e.g `ITLSTORCHBURNING.ZEN`.
  
**Returns `userdata`:**

address the address of loaded parent vob from vobtree.

----
### searchVobByName

This method will search for vob by it's object name in selected world.

```cpp
userdata searchVobByName(string objectName)
```

**Parameters:**

* `string` **objectName**: the name of the vob object.
  
**Returns `userdata`:**

address the address of found vob.

----
### searchVobListByName

This method will search for vobs with given object name in selected world.

```cpp
array[userdata] searchVobListByName(string objectName)
```

**Parameters:**

* `string` **objectName**: the name of the vob object.
  
**Returns `array[userdata]`:**

addresses the array of found vobs.

----
### traceRayFirstHit
!!! note
    In order to detect collision with npc(s), you should pass these two flags: `TRACERAY_VOB_BBOX` | `TRACERAY_VOB_IGNORE_NO_CD_DYN`.

This method will cast a ray from origin point in direction, against all colliders specified by the flags and returns first intersection result.

```cpp
null|TraceRayReport traceRayFirstHit(Vec3 origin, Vec3 direction, int flags)
```

**Parameters:**

* `Vec3` **origin**: the starting point of ray in world coordinates.
* `Vec3` **direction**: the direction of the ray.
* `int` **flags**: the flags defining which colliders can ray intersect (e.g: poly, vob, npc, bbox). For more information see [TraceRay Constants](../../../client-constants/traceray/).
  
**Returns `null|TraceRayReport`:**

report containing results of ray intersection, or `null` on failure. For more information see [TraceRayReport](../../../client-classes/game/TraceRayReport/).

----
### traceRayNearestHit
!!! note
    In order to detect collision with npc(s), you should pass these two flags: `TRACERAY_VOB_BBOX` | `TRACERAY_VOB_IGNORE_NO_CD_DYN`.

This method will cast a ray from origin point in direction, against all colliders specified by the flags and returns nearest intersection result.

```cpp
null|TraceRayReport traceRayNearestHit(Vec3 origin, Vec3 direction, int flags)
```

**Parameters:**

* `Vec3` **origin**: the starting point of ray in world coordinates.
* `Vec3` **direction**: the direction of the ray.
* `int` **flags**: the flags defining which colliders can ray intersect (e.g: poly, vob, npc, bbox). For more information see [TraceRay Constants](../../../client-constants/traceray/).
  
**Returns `null|TraceRayReport`:**

report containing results of ray intersection, or `null` on failure. For more information see [TraceRayReport](../../../client-classes/game/TraceRayReport/).

----
