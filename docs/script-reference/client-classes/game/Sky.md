---
title: 'Sky'
---
# `static class` Sky <font size="4">(client-side)</font>
!!! info "Available since version: 0.1.6"

This class represents in game Sky.


## Properties
### `int` weather 
!!! info "Available since version: 0.1.10"

Represents the sky weather. For more information see [Weather constants](../../../shared-constants/weather).

----
### `bool` raining 
!!! info "Available since version: 0.1.10"

Represents the raining/snowing state.

----
### `bool` renderLightning 
!!! info "Available since version: 0.1.10"

Represents the lightning feature during raining state.  
Lightning will only be rendered during raining and when weatherWeight is larger than 0.5

----
### `float` windScale 
!!! info "Available since version: 0.1.10"

Represents the sky wind scale used during raining/snowing.

----
### `float` weatherWeight <font size="2">(read-only)</font>
!!! info "Available since version: 0.1.10"

Represents the sky weather condition development in range 0.0 to 1.0.  
When it starts raining/snowing this value gradually increases to 1.0.  
When it stops raining/snowing this value gradually decreases back to near 0.0.

----
### `bool` dontRain 
!!! info "Available since version: 0.1.10"

Represents the sky dontRain feature.  
When it's enabled, the rain/snow won't fall.

----
### `bool` darkSky 
!!! info "Available since version: 0.1.10"

Represents the dark sky feature.
When it's enabled, the sun clouds are dark.

----

## Methods
### setRainStartTime
!!! info "Available since version: 0.1.10"

This method will set the sky weather time when it starts raining/snowing.

```cpp
void setRainStartTime(int hour, int min)
```

**Parameters:**

* `int` **hour**: the sky weather raining start hour.
* `int` **min**: the sky weather raining start min.
  

----
### getRainStartTime
!!! info "Available since version: 0.1.10"

This method will get the sky weather time when it starts raining/snowing.

```cpp
{hour, min} getRainStartTime()
```

  
**Returns `{hour, min}`:**

the sky weather raining start time.

----
### setRainStopTime
!!! info "Available since version: 0.1.10"

This method will set the sky weather time when it stops raining/snowing.

```cpp
void setRainStopTime(int hour, int min)
```

**Parameters:**

* `int` **hour**: the sky weather raining stop hour.
* `int` **min**: the sky weather raining stop min.
  

----
### getRainStopTime
!!! info "Available since version: 0.1.10"

This method will get the sky weather time when it stops raining/snowing.

```cpp
{hour, min} getRainStopTime()
```

  
**Returns `{hour, min}`:**

the sky weather raining stop time.

----
### setFogColor

This method will set the sky fog color day variation.

```cpp
void setFogColor(int id, int r, int g, int b)
```

**Parameters:**

* `int` **id**: the id of fog color day variation.
* `int` **r**: the red color component in RGB model.
* `int` **g**: the green color component in RGB model.
* `int` **b**: the blue color component in RGB model.
  

----
### setCloudsColor

This method will set the sky clouds color.

```cpp
void setCloudsColor(int r, int g, int b)
```

**Parameters:**

* `int` **r**: the red color component in RGB model.
* `int` **g**: the green color component in RGB model.
* `int` **b**: the blue color component in RGB model.
  

----
### setPlanetSize

This method will set the planet size ratio.

```cpp
void setPlanetSize(int planetId, float size)
```

**Parameters:**

* `int` **planetId**: the planet id, for more information see [Planet constants](../../../client-constants/planet/).
* `float` **size**: the size ratio.
  

----
### setPlanetColor

This method will set the planet color.

```cpp
void setPlanetColor(int planetId, int r, int g, int b, int a)
```

**Parameters:**

* `int` **planetId**: the planet id, for more information see [Planet constants](../../../client-constants/planet/).
* `int` **r**: the red color component in RGBA model.
* `int` **g**: the green color component in RGBA model.
* `int` **b**: the blue color component in RGBA model.
* `int` **a**: the alpha color component in RGBA model.
  

----
### setPlanetTxt
!!! info "Available since version: 0.3.0"

This method will set the planet texture.

```cpp
void setPlanetTxt(int planetId, string texture)
```

**Parameters:**

* `int` **planetId**: the planet id, for more information see [Planet constants](../../../client-constants/planet/).
* `string` **texture**: name.
  

----
### setLightingColor

This method will set the sky lighting color.

```cpp
void setLightingColor(int r, int g, int b)
```

**Parameters:**

* `int` **r**: the red color component in RGB model.
* `int` **g**: the green color component in RGB model.
* `int` **b**: the blue color component in RGB model.
  

----
