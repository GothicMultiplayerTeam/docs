---
title: 'CollisionReport'
---
# `class` CollisionReport <font size="4">(client-side)</font>
!!! info "Available since version: 0.3.0"

This class represents collision report (collision result of a pair of objects).


## Properties
### `Vec3&` contactPoint 

Represents collision contact point.

----
### `Vec3&` collNormal0 

Represents the first normal collision point.

----
### `Vec3&` collNormal1 

Represents the second normal collision point.

----
### `userdata` vob0 <font size="2">(read-only)</font>

Represents the first vob pointer.

----
### `userdata` vob1 <font size="2">(read-only)</font>
!!! note
    Might be empty if object collide with world mesh

Represents the second vob pointer.

----

## Methods
No methods.

----
