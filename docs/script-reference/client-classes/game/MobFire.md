---
title: 'MobFire'
---
# `class` MobFire `extends` [MobInter](../MobInter/) <font size="4">(client-side)</font>
!!! info "Available since version: 0.3.0"

This class represents fire object in the world.

### Constructor
```cpp
MobFire(string model)
```

**Parameters:**

* `string` **model**: the model name to be used as visual.
### Constructor
```cpp
MobFire(userpointer ptr)
```

**Parameters:**

* `userpointer` **ptr**: the pointer to the object from the game.

## Properties
### `string` fireVobtreeName 

Represents the fire vobtree name. Example: "FIRETREE_LAMP.ZEN"

----
### `string` fireSlot 

Represents the fire slot. Example: "BIP01 FIRE"

----

## Methods
No methods.

----
