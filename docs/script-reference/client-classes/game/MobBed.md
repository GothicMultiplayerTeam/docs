---
title: 'MobBed'
---
# `class` MobBed `extends` [MobInter](../MobInter/) <font size="4">(client-side)</font>
!!! info "Available since version: 0.3.0"

This class represents bed object in the world.

### Constructor
```cpp
MobBed(string model)
```

**Parameters:**

* `string` **model**: the model name to be used as visual.
### Constructor
```cpp
MobBed(userpointer ptr)
```

**Parameters:**

* `userpointer` **ptr**: the pointer to the object from the game.

## Properties
No properties.

----

## Methods
No methods.

----
