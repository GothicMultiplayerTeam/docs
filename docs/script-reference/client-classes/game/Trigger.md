---
title: 'Trigger'
---
# `class` Trigger `extends` [Vob](../Vob/) <font size="4">(client-side)</font>
!!! info "Available since version: 0.2.1"

This class represents world trigger, which can activate various things.

### Constructor
```cpp
Trigger()
```

**Parameters:**

No parameters.
### Constructor
```cpp
Trigger(userpointer ptr)
```

**Parameters:**

* `userpointer` **ptr**: the pointer to the object from the game.

## Properties
### `bool` reactToOnTrigger 

Represents the status if `trigger action` should be invoked on target vob.

----
### `bool` reactToOnTouch 

Represents the status if `touch action` should be invoked on target vob.

----
### `bool` reactToOnDamage 

Represents the status if `damage action` should be invoked on target vob.

----
### `bool` respondToObject 

Represents the status if `trigger action` should be invoked on vob.

----
### `bool` respondToPC 

Represents the status if `trigger action` should be invoked on player.

----
### `bool` respondToNPC 

Represents the status if `trigger action` should be invoked on npc.

----
### `bool` startEnabled 

Represents the initial value of the `isEnabled` property that is loaded from ZEN file.

----
### `bool` isEnabled 

Represents the status if any trigger actions will be invoked on the trigger.

----
### `bool` sendUntrigger 

Represents the status if `untrigger action` will be invoked on the trigger.

----
### `string` respondToVobname 

Represents the vob name that has to be the same as the `triggerTarget` vob name in order for trigger actions to be invoked on it.

----
### `short` numCanBeActivated 

Represents the initial value of the `countCanBeActivated` property that is loaded from ZEN file.

----
### `float` retriggerWaitSec 

Represents the delay time in seconds in which the trigger will reinvoke any trigger event.

----
### `float` damageThreshold 

Represents the minimum damage value that need to be passed in trigger `damage action` in order, to invoke the trigger.

----
### `float` fireDelaySec 

Represents the delay time in seconds in which the trigger will invoke any trigger event.

----
### `float` nextTimeTriggerable 

Represents the timestamp in miliseconds in which the trigger will be triggered next. The `retriggerWaitSec` is used in setting the next value for this property.

----
### `userpointer` savedOtherVob <font size="2">(read-only)</font>

Represents the vob ptr passed in activateTrigger method that will be triggered after `fireDelaySec` field value.

----
### `short` countCanBeActivated 

Represents the number of how many times the trigger will be activated.

----

## Methods
### activateTrigger
!!! note
    In order to invoke this action trigger must be added to game world.

This method will activate the trigger if conditions are met and cache the instigatorVob to `savedOtherVob` field if `fireDelaySec` is larger than 0.

```cpp
void activateTrigger(userpointer instigatorVob = null)
```

**Parameters:**

* `userpointer` **instigatorVob**: the vob that is used by some of engine classes. Can be ommited in most cases.
  

----
### deactiveTrigger
!!! note
    In order to invoke this action trigger must be added to game world.

This method will deactivate the trigger if conditions are met.

```cpp
void deactiveTrigger(userpointer instigatorVob = null)
```

**Parameters:**

* `userpointer` **instigatorVob**: the vob that is used by some of engine classes. Can be ommited in most cases.
  

----
