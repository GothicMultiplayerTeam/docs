---
title: 'Line'
---
# `class` Line <font size="4">(client-side)</font>
!!! info "Available since version: 0.3.0"

This class represents 2d line visible on the screen.

### Constructor
```cpp
Line(int x1, int y1, int x2, int y2)
```

**Parameters:**

* `int` **x1**: the beginning x position of the line in virtuals.
* `int` **y1**: the beginning y position of the line in virtuals.
* `int` **x2**: the end x position of the line in virtuals.
* `int` **y2**: the end y position of the line in virtuals.

## Properties
### `bool` visible 

Represents the visibility state of the line.

----
### `Color&` color 

Represents the line color in RGBA model.

----

## Methods
### top

This method will move the line at the end of the render queue.
It will be visible at the top of the screen (other elements will be covered by it).

```cpp
void top()
```

  

----
### setBeginPosition

This method will set the beginning point of the line in virtuals.

```cpp
void setBeginPosition(int x, int y)
```

**Parameters:**

* `int` **x**: the x position in virtuals.
* `int` **y**: the y position in virtuals.
  

----
### setBeginPositionPx

This method will set the beginning point of the line in pixels.

```cpp
void setBeginPositionPx(int x, int y)
```

**Parameters:**

* `int` **x**: the x position in pixels.
* `int` **y**: the y position in pixels.
  

----
### setEndPosition

This method will set the end point of the line in virtuals.

```cpp
void setEndPosition(int x, int y)
```

**Parameters:**

* `int` **x**: the x position in virtuals.
* `int` **y**: the y position in virtuals.
  

----
### setEndPositionPx

This method will set the end point of the line in pixels.

```cpp
void setEndPositionPx(int x, int y)
```

**Parameters:**

* `int` **x**: the x position in pixels.
* `int` **y**: the y position in pixels.
  

----
### getBeginPosition

This method will get the position of the beginning point of the line in virtuals.

```cpp
Vec2i getBeginPosition()
```

  
**Returns `Vec2i`:**

the beginning position of the line on screen in virtuals.

----
### getBeginPositionPx

This method will get the position of the beginning point of the line in pixels.

```cpp
Vec2i getBeginPositionPx()
```

  
**Returns `Vec2i`:**

the beginning position of the line on screen in pixels.

----
### getEndPosition

This method will get the position of the end point of the line in virtuals.

```cpp
Vec2i getEndPosition()
```

  
**Returns `Vec2i`:**

the end position of the line on screen in virtuals.

----
### getEndPositionPx

This method will get the position of the end point of the line in pixels.

```cpp
Vec2i getEndPositionPx()
```

  
**Returns `Vec2i`:**

the end position of the line on screen in pixels.

----
