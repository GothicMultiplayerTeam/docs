---
title: 'Texture'
---
# `class` Texture <font size="4">(client-side)</font>
!!! info "Available since version: 0.1.0"

This class represents texture visible on the screen.

### Constructor
```cpp
Texture(int x, int y, int width, int height, string txt)
```

**Parameters:**

* `int` **x**: the x position in virtuals.
* `int` **y**: the y position in virtuals.
* `int` **width**: the width size in virtuals.
* `int` **height**: the height size in virtuals.
* `string` **txt**: the texture file to display.

## Properties
### `bool` visible 

Represents the visibility state of the texture.

----
### `string` file 

Represents the file name of displayed image on texture.

----
### `Color&` color 
!!! info "Available since version: 0.3.0"

Represents the texture color in RGBA model.

----
### `float` rotation 
!!! info "Available since version: 0.1.10"

Represents the rotation of the texture in degrees.  
The rotation is performed clockwise around the texture pivot point.

----

## Methods
### top

This method will move the texture at the end of the render queue.  
It will be visible at the top of the screen (other elements will be covered by it).

```cpp
void top()
```

  

----
### setPosition

This method will set the texture position on screen in virtuals.

```cpp
void setPosition(int x, int y)
```

**Parameters:**

* `int` **x**: the x position in virtuals.
* `int` **y**: the y position in virtuals.
  

----
### setPositionPx

This method will set the texture position on screen in pixels.

```cpp
void setPositionPx(int x, int y)
```

**Parameters:**

* `int` **x**: the x position in pixels.
* `int` **y**: the y position in pixels.
  

----
### getPosition

This method will get the texture position on screen in virtuals.

```cpp
Vec2i getPosition()
```

  
**Returns `Vec2i`:**

the texture position on screen.

----
### getPositionPx

This method will get the texture position on screen in pixels.

```cpp
Vec2i getPositionPx()
```

  
**Returns `Vec2i`:**

the texture position on screen.

----
### setPivotPoint
!!! info "Available since version: 0.1.10"

This method will set the pivot point of the texture in virtuals (Offset from the center of it).  
All rotations will be done relative to this point.

```cpp
void setPivotPoint(int x, int y)
```

**Parameters:**

* `int` **x**: the x offset in virtuals.
* `int` **y**: the y offset in virtuals.
  

----
### setPivotPointPx
!!! info "Available since version: 0.1.10"

This method will set the pivot point of the texture in pixels (Offset from the center of it).  
All rotations will be done relative to this point.

```cpp
void setPivotPointPx(int x, int y)
```

**Parameters:**

* `int` **x**: the x offset in pixels.
* `int` **y**: the y offset in pixels.
  

----
### getPivotPoint
!!! info "Available since version: 0.1.10"

This method will get the pivot point of the texture in virtuals (Offset from the center of it).

```cpp
Vec2i getPivotPoint()
```

  
**Returns `Vec2i`:**

the offset in virtuals. The default pivot point value is `{x=0,y=0}`

----
### getPivotPointPx
!!! info "Available since version: 0.1.10"

This method will get the pivot point of the texture in pixels (Offset from the center of it).

```cpp
Vec2i getPivotPointPx()
```

  
**Returns `Vec2i`:**

the offset in pixels. The default pivot point value is `{x=0,y=0}`

----
### setSize

This method will set the texture size on screen in virtuals.

```cpp
void setSize(int width, int height)
```

**Parameters:**

* `int` **width**: the width size in virtuals.
* `int` **height**: the height size in virtuals.
  

----
### setSizePx

This method will set the texture size on screen in pixels.

```cpp
void setSizePx(int width, int height)
```

**Parameters:**

* `int` **width**: the width size in pixels.
* `int` **height**: the height size in pixels.
  

----
### getSize

This method will get the texture size on screen in virtuals.

```cpp
Vec2i getSize()
```

  
**Returns `Vec2i`:**

the texture size on screen.

----
### getSizePx

This method will get the texture size on screen in pixels.

```cpp
Vec2i getSizePx()
```

  
**Returns `Vec2i`:**

the texture size on screen.

----
### setUV
!!! info "Available since version: 0.1.10"

This method will set the mapping coordinates (UV) of the texture.

```cpp
void setUV(float x, float y, float width, float height)
```

**Parameters:**

* `float` **x**: the u position of image on texture (Default 0.0).
* `float` **y**: the v position of image on texture (Default 0.0).
* `float` **width**: the u size of image on texture (Default 1.0).
* `float` **height**: the v size of image on texture (Default 1.0).
  

----
### getUV
!!! info "Available since version: 0.1.10"

This method will get the mapping coordinates (UV) of the texture.

```cpp
{x, y, width, height} getUV()
```

  
**Returns `{x, y, width, height}`:**

the position and size of image on texture.

----
