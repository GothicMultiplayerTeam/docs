## Compiling

### Windows

In order to compile one of the sample projects described in [about article](../about), you need to install:

1. [**Visual Studio**](https://visualstudio.microsoft.com) (at least 2022 version)
2. **CMake tools for Visual Studio** (you can install this from Visual Studio installer)

After installing required software, open up the project directory via Visual Studio.  
The rest can be done from the Visual Studio UI.

### Linux

In order to compile the [**Squirrel Module Template**](https://gitlab.com/GothicMultiplayerTeam/modules/squirrel-template) project you need to install:

```
sudo apt install -y build-essential cmake
```

After installing required packages you have to configure cmake project with certain preset.  
Checkout `CMakePreset.json` file for available presets.  
Be sure to pick the preset that matches host architecture under you build the project.

```
cmake . --preset (preset_name)
```

To compile the project you need to type this command:

```
cmake --build . --preset (preset_name)
```

## Binaries location

The binaries can be located in `${project_directory}/out/build/${preset_name}`.

## Avoid copying binaries

In order to avoid constantly copying files whenever you make a new build, you can create a symbolic link.
Symlinks allow to point to specific file in specific location, without the need of having such file physically located there.

### Windows

You can either use this command in `cmd.exe`:
```
mklink "absolute/path/to/symlink" "absolute/path/to/file"
```

Or [Link Shell Extension](https://schinagl.priv.at/nt/hardlinkshellext/linkshellextension.html) that allows you to easilly create new symlinks by using context menu.


### Linux

You can create a new symlink by typing this command in your `shell`:
```
ln -s "absolute/path/to/file" "absolute/path/to/symlink"
```