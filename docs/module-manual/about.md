**Gothic 2 Online** allows you to further extends squirrel capabilities by creating prebuilt modules.  
Such modules are written in **C++**, g2o provides example projects that can be used to create your own module.

#### Squirrel Module Template

This project uses [**CMake**](https://cmake.org/) to compile code for various systems and CPU architectures supported by g2o.
The minimum required CMake version is `3.21`.

It also provides a preconfigured [**gitab-ci**](https://docs.gitlab.com/ee/ci/) for repositories hosted on [**gitlab**](https://gitlab.com/).  
By using it, you can build and release the module for every supported architecture.

Another useful thing included in this project is the ability to generate/deploy docs via **gitlab-ci**.  
The template uses the [**docsgenerator**](https://gitlab.com/GothicMultiplayerTeam/docsgenerator) for that.  
By including special comments in `header`/`source` files located `src/` directory, you can easilly document your code.

#### Union Module Template

This project extends the **Squirrel Module Template** by using [**Union Framework**](https://gitlab.com/union-framework) for game memory hacking.

It exclusively supports **32-bit Windows** due to the compilation architecture of **Gothic 2**.