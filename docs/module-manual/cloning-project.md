Because the project itself uses the [**git submodules**](https://git-scm.com/book/en/v2/Git-Tools-Submodules),   
you need to pass the `--recursive` parameter to the `git clone` command:
```
git clone https://gitlab.com/GothicMultiplayerTeam/modules/squirrel-template.git --recursive
```