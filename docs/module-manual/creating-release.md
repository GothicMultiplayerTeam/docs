By using [**gitab-ci**](https://docs.gitlab.com/ee/ci/) you can create a new release for your module.  
In order to create a new release, you need to follow these steps:

1. Edit the `.gitlab-ci.yml` file, by changing the VERSION variable to the desired version
```yml
variables:
  GIT_SUBMODULE_STRATEGY: recursive

  PROJECT_NAME: ${CI_PROJECT_NAME}
  VERSION: "0.1" # Make sure that the version text is unique
```

2. Edit the `CHANGELOG.md` file, put your own list of changes
```md
## Changelog

- Fixed some error
- Added new feature
```

3. Create the new [**pipeline**](https://docs.gitlab.com/ee/ci/pipelines/) via **gitlab-ui**
![gif showing how to run pipeline via gitlab ui](../assets/module-manual/create-pipeline.gif)

4. Run the build job (this will build module for each preset + build the documentation)  
Building takes about from 5 to 10 minutes, so be patient!
![gif showing how to run build job via gitlab ui](../assets/module-manual/run-build-job.gif)

5. Run the release job (this will create a new release in **gitlab-ui**)
![gif showing how to run release job via gitlab ui](../assets/module-manual/run-release-job.gif)

6. View your newly created release (once the release job finishes)
![gif showing how to view releases page via gitlab ui](../assets/module-manual/view-release.gif)