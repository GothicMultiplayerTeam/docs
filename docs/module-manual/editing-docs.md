The project uses the [MkDocs](https://www.mkdocs.org/) docs generator.  
The **mkdocs** allows you to create articles that are written using **markdown**.

## Basic configuration

You should start from configuring the documentation info by editing `docs/mkdocs.yml` file
```yml
site_name: Squirrel Template Module - Docs # the name of the documentation site
repo_url: https://gitlab.com/GothicMultiplayerTeam/modules/squirrel-template # the link that will be redirecting to your repo
repo_name: Squirrel Template Module # the repository name
```

To change the documentation logo, you have to replace `docs/assets/logo.png` file with your own.

## Using docs locally

### Installing python

It's also possible to setup the docs on your local pc, to do that, first, you need to install [**python**](https://www.python.org/downloads/).

### Getting docsgenerator

Now, you should clone the [**docsgenerator**](https://gitlab.com/GothicMultiplayerTeam/docsgenerator) into `your/repo/path/docs/` directory.

### Installing required python dependencies via pip

Next step involes installing required dependencies by the **docs** and **docsgenerator**.  
Open up the command prompt and navigate to the `your/repo/path/docs/` subfolder, and type the following commands:
```
pip install -r docs/requirements.txt
pip install -r docsgenerator/requirements.txt
```

### Generating the documentation

In order to generate the special comments into actual .md articles, you need to run the **docsgenerator**:
```
python docsgenerator/generate.py 0.1 ../src --builddir docs/script-reference --markdown
```
I highly recommend to put this command into `docs/generate.bat` file, since you'll propably be using it quite often.  
You should run this script each time, you update the documentation comments inside your code.

### Running the documentation

To start the mkdocs server, you need to type this commands:
```
Title mkdocs server
mkdocs serve
```
I highly recommend to put this command into `docs/serve.bat` file, since you'll propably be using it quite often.

Now with server running, you can access the documentation under this address: [https://127.0.0.1:8000](https://127.0.0.1:8000).  
You should also create a web shortcut for this domain for convenience in `/docs/site.url`.

### Ignoring files in git

If you don't want to be bother with **git** detecting newly added files as a untracked changes, you can ignore them only for your repo,
to do that, go to the `your/repo/path/.git/info/` directory, and edit the `exclude` file located there, add the following lines:
```
/docs/script-reference/
/docs/docsgenerator/

/docs/generate.bat
/docs/serve.bat
/docs/site.url
```